import "bootstrap/dist/css/bootstrap.min.css";
import React, { useEffect, useState } from 'react';
import { Router, useHistory, useLocation } from "react-router-dom";
import './App.css';
import AuthenticationComponent from "./components/AuthenticationComponent";
import SystemComponent from "./components/SystemComponent";




import firebase from "firebase/app";
import "firebase/auth";

import "dotenv";


// For Firebase JS SDK v7.20.0 and later, measurementId is optional



firebase.initializeApp({
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID,
  measurementId: process.env.REACT_APP_MEASUREMENT_ID
})











const App = (props) => {
  const history = useHistory();
  const [loading, setLoading] = useState(true);
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [user, setUser] = useState(null);
  const {component:Component, ...rest} = props;
  
  var db = firebase.firestore();
  var functions = firebase.functions();
  var auth = firebase.auth();


  

  if(window.location.hostname === "localhost"){
    console.log("THIS IS A DEVELOPMENT ENVIRONMENT");
    db.useEmulator('localhost', 8080);
    functions.useEmulator('localhost', 5001);
    auth.useEmulator('http://localhost:9099');
  } 

  

    
  
    
  
    useEffect(() => {
      setIsAuthenticated(firebase.auth().currentUser !== null)
      setUser(firebase.auth().currentUser)
      firebase.auth().onAuthStateChanged((user) => {
        if(user){
          setIsAuthenticated(true)
        } else {
          setIsAuthenticated(false)
        }
      })
    },[])

    

    

    

  if(!isAuthenticated){
    console.log("Navigating to Authentication");
    return (<Router history={history} >
      
      <div className="App">
        
        <AuthenticationComponent/>
    </div>   
    </Router>
    );
  } else {
    console.log("Navigating to Contyfy.com");
    return (<Router history={history}>
  
      <div className="App"> 
      
      <SystemComponent />
    </div>   
    </Router>
    );
  }

      
       
    
        
      
    
    
  
}


export default App;
