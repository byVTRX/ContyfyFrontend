import React, {useEffect, useState} from "react";

import logo from "../Logo.png"
import {List, Grid, makeStyles, Tabs, Tab, Typography, ListItem, Card, Avatar, Button} from "@material-ui/core";
import { Link, useHistory, useLocation } from "react-router-dom";
import "./main.css"


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 280,

    
    
    marginLeft:"9%",
    textAlign:"end",
    marginTop:"50px",
    marginRight:"50px",
    
    textDecorationColor:"white",
    display:"flex",
    flexDirection:"column",
    position:"fixed",
    
    borderRadius:"20px",
    background:"transparent",
    [theme.breakpoints.down('sm')]:{
      display:"none"
    },
    
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  navItem:{
    fontSize:"20px",
    fontFamily: "'Poppins', sans-serif",
    display:"flex",
    heigth:"100%",
    margin:"20px auto",
    justifyContent:"left",
    alignItems:"center",
    "&:hover":{
      background:"none"
    },
    "&:active:after":{
      background:"none",
      color:"transparent"
    },
    "&:after":{
      background:"none",
      color:"transparent"
    }
  },
  avatar: {
    backgroundColor: "white",
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
  asideText: {
    fontSize:"20px",
    margin:"auto 20px"
  },
  innerNavItem: {
    "&:hover":{
      background: 'linear-gradient(90deg, rgba(255,146,1,1) 0%, rgba(251,34,34,1) 81%);',
      borderRadius:"20px",

    },
    maxWidth:"100%",
    verticalAlign:"middle",
    textAlign:"left", 
    
    
  },
  
}));
export default function Aside(props) {
  const classes = useStyles();
  const [value, setValue] = useState("");
  const history = useHistory();
  const location = useLocation();
  const user = props.user;
 
 
  useEffect(() => {
    if(location.pathname.replace('/', '') === ""){
      setValue('home')
    } else {
      if(location.pathname.replace('/', '') === "create-post"){
        setValue('')
      } else if(location.pathname.replace('/','').startsWith('profile/')) {
        setValue('')
      } else if(location.pathname.replace('/','').startsWith('dashboard')){
        setValue('dashboard')
      } else if(location.pathname.replace('/','').startsWith('directmessages')) {
        setValue('directmessages');
      } else if(location.pathname.replace('/','').startsWith('payment')) {
        setValue('payment');
      } else if(location.pathname.replace('/','').startsWith('profile')) {
        setValue('profile');
      } else if(location.pathname.replace('/','').startsWith('notifications')) {
        setValue('notifications');
      } else {
        setValue('')
      }
      
    }

    

    if(value !== ""){
      console.log('Active element:' + value)
      document.getElementById(value).style = "background: linear-gradient(90deg, rgba(255,146,1,1) 0%, rgba(251,34,34,1) 81%); border-radius:20px; color:white;text-decoration:none";
    
    } 
    
   
  },[value])

  

  
  
  
    return (
      <Card className={classes.root} elevation={0}>
      <List
      component="nav"
      aria-labelledby="list-subheader"
      
      >
        <ListItem className={classes.navItem} button onClick={(e) => {e.preventDefault()
          history.push("/"); setValue("home")}}>
          <Link className={classes.innerNavItem} id="home" style={{color:"white", textDecoration:"none"}} ><div className={classes.asideText}><i className="fas fa-home text-center" style={{fontSize:"25px", width:"30px", height:"30px"}}/> <span >Home</span></div></Link>
        </ListItem>
        <ListItem className={classes.navItem} button onClick={(e) => {e.preventDefault()
          history.push("/notifications"); setValue('notifications')}}>
          <Link className={classes.innerNavItem} id="notifications" style={{color:"white", textDecoration:"none"}} ><div className={classes.asideText}><i className="far fa-bell text-center" style={{fontSize:"25px",width:"30px", height:"30px"}}/>  <span >Notifications</span></div></Link>
        </ListItem>
        <ListItem className={classes.navItem} button onClick={(e) => {e.preventDefault()
          history.push("/directmessages"); setValue('directmessages')}}>
          <Link className={classes.innerNavItem} id="directmessages"style={{color:"white", textDecoration:"none"}} ><div className={classes.asideText}><i className="fas fa-paper-plane text-center" style={{fontSize:"25px",width:"30px", height:"30px",fill:"url(linear-gradient(90deg, rgba(255,146,1,1) 0%, rgba(251,34,34,1) 81%))"}}/>  <span >Direct Messages</span></div></Link>
        </ListItem>
        <ListItem className={classes.navItem} button onClick={(e) => {e.preventDefault()
          history.push("/dashboard"); setValue('dashboard')}}>
          <Link className={classes.innerNavItem} id="dashboard"style={{color:"white", textDecoration:"none"}} ><div className={classes.asideText}><i className="far fa-compass text-center" style={{fontSize:"25px",width:"30px", height:"30px",fill:"url(linear-gradient(90deg, rgba(255,146,1,1) 0%, rgba(251,34,34,1) 81%))"}}/>  <span >Dashboard</span></div></Link>
        </ListItem>
        <ListItem className={classes.navItem} button onClick={(e) => {e.preventDefault()
          history.push("/payment"); setValue('payment')}}>
          <Link className={classes.innerNavItem} id="payment"style={{color:"white", textDecoration:"none"}} ><div className={classes.asideText}><i className="fas fa-dollar-sign text-center" style={{fontSize:"25px",width:"30px", height:"30px"}}/>  <span >Payment</span></div></Link>
        </ListItem>
        <ListItem className={classes.navItem} button onClick={(e) => {e.preventDefault()
          history.push("/profile"); setValue('profile')}}>
          <Link className={classes.innerNavItem} id="profile"style={{color:"white", textDecoration:"none"}} ><div className={classes.asideText}><i className="fas fa-user text-center" style={{fontSize:"25px",width:"30px", height:"30px"}}/>  <span style={{marginLeft:"4px"}}>Profile</span></div></Link>
        </ListItem>
        <ListItem className={classes.navItem} style={{marginTop:"100%", display:"inline-flex"}} button >
        <div className={classes.innerNavItem} style={{display:"inline-flex", flexDirection:"row", padding:"10px", paddingRight:"100%", borderRadius:"30px"}} type="button" data-toggle="popover" data-trigger="click" data-placement="top"  data-html="true" data-content=
        "<ul class='list-group list-group-flush bg-dark'><li class='list-group-item'><a class='btn' href='/settings'><i class='flaticon2-settings'></i>Account Settings</a></li><li class='list-group-item'><a class='btn' href='/logout'><i class='flaticon2-cancel'></i>Logout</a></li></ul>">
          <Avatar>{user.profilepicture && <img src={user.profilepicture} style={{objectFit:"cover"}} height="100%" width="100%" alt={user.username.charAt(0)}/>}</Avatar>
          <div style={{marginLeft:"10px", alignItems:"center"}}> 
          <Typography variant="h6" color="textPrimary" style={{margin:0, padding:0, color:"white", width:"100px", fontSize:"14px"}}>{user.displayName}</Typography>
          <Typography variant="subtitle1" className="text-muted" style={{margin:0, padding:0, fontSize:"11px"}}>@{user.username}</Typography>
          </div>
          
        </div>
        </ListItem>
      </List>
      </Card>
     
    
     

  )
  
  
}





