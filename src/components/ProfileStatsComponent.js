import React, { useState, useEffect, useReducer } from "react";
import {
    Button,
    List,
    ListItem,
    Divider,
    ListItemText,
    ListItemAvatar,
    Avatar,
    Typography,
    CircularProgress,
    Card,
    CardContent,
    CardHeader,
    CardActions,
    IconButton,
    CardMedia,
    Collapse,
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

import logo from "../Logo.png"
import verified from "../Verification.svg"

const useStyles = makeStyles(theme => ({
    root: {
        
            marginTop: "5%", 
            marginLeft: "10px", 
            height: "380px", 
            maxWidth: "320px", 
            background: "#282a30", 
            border: "1px solid white", 
            display: "flex",
        [theme.breakpoints.down("sm")]:{
            display:"none"
        }
    }
}))

const ProfileStats = () => {
    const classes = useStyles();

    return (
        <Card className={classes.root} elevation={3} >
            <CardContent style={{ margin: 0, paddingTop: 0, paddingBottom: 0, paddingLeft: 10, paddingRight: 10 }}>
                <List>
                    <ListItem>
                        <ListItemAvatar >


                            <i className="fas fa-angle-double-up" style={{ marginLeft: "15px", fontSize: "20px" }} />

                        </ListItemAvatar>
                        <ListItemText className={classes.liText} primary={
                            <Typography variant="h5" component="p">
                                Rank: #1
                            </Typography>
                        }></ListItemText>
                    </ListItem>
                    <Divider />
                    <ListItem>
                        <ListItemAvatar>


                            <i className="far fa-star" style={{ marginLeft: "10px", fontSize: "20px" }} />

                        </ListItemAvatar>
                        <ListItemText className={classes.liText} primary={
                            <Typography variant="h5" component="p">
                                4.140.230 people currently subscribed
                            </Typography>
                        }></ListItemText>
                    </ListItem>
                    <Divider />
                    <ListItem>
                        <ListItemAvatar>

                            <img src={logo} width="24px" height="24px" style={{ marginLeft: "10px" }} />


                        </ListItemAvatar>
                        <ListItemText className={classes.liText} primary={
                            <Typography variant="h5" component="p">
                                Creator since 01/01/2021
                            </Typography>
                        }></ListItemText>
                    </ListItem>
                    <Divider />
                    <ListItem>
                        <ListItemAvatar className="text-left">

                            <img width="24px" heigth="24px" style={{ marginLeft: "10px" }} className="center text-center" src={verified} />


                        </ListItemAvatar>
                        <ListItemText className={classes.liText} primary={
                            <Typography variant="h5" component="p">
                                Official Contyfy partner
                            </Typography>
                        } ></ListItemText>
                    </ListItem>
                    <Divider />

                    <ListItem className="row justify-content-between" style={{ width: "100%", padding: 0, margin: 0, marginTop: "10px", }}>

                        <ListItemAvatar className="text-center" style={{ width: "30%" }}>

                            <span className="svg-icon svg-icon-white svg-icon-2x">{/*begin::Svg Icon | path:/var/www/preview.keenthemes.com/keen/releases/2021-02-02-111420/theme/demo1/dist/../src/media/svg/icons/Home/Picture.svg*/}<svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24" />
                                    <rect fill="#000000" opacity="0.3" x={2} y={4} width={20} height={16} rx={2} />
                                    <polygon fill="#000000" opacity="0.3" points="4 20 10.5 11 17 20" />
                                    <polygon fill="#000000" points="11 20 15.5 14 20 20" />
                                    <circle fill="#000000" opacity="0.3" cx="18.5" cy="8.5" r="1.5" />
                                </g>
                            </svg>{/*end::Svg Icon*/}</span>

                            <Typography variant="h5" color="textPrimary">20</Typography>

                        </ListItemAvatar>
                        <Divider orientation="vertical" style={{ height: "90px", margin: 0 }} />
                        <ListItemAvatar className="text-center" style={{ width: "30%" }}>

                            <span className="svg-icon svg-icon-white svg-icon-2x">{/*begin::Svg Icon | path:/var/www/preview.keenthemes.com/keen/releases/2021-02-02-111420/theme/demo1/dist/../src/media/svg/icons/Devices/Video-camera.svg*/}<svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <rect x={0} y={0} width={24} height={24} />
                                    <rect fill="#000000" x={2} y={6} width={13} height={12} rx={2} />
                                    <path d="M22,8.4142119 L22,15.5857848 C22,16.1380695 21.5522847,16.5857848 21,16.5857848 C20.7347833,16.5857848 20.4804293,16.4804278 20.2928929,16.2928912 L16.7071064,12.7071013 C16.3165823,12.3165768 16.3165826,11.6834118 16.7071071,11.2928877 L20.2928936,7.70710477 C20.683418,7.31658067 21.316583,7.31658098 21.7071071,7.70710546 C21.8946433,7.89464181 22,8.14899558 22,8.4142119 Z" fill="#000000" opacity="0.3" />
                                </g>
                            </svg>{/*end::Svg Icon*/}</span>

                            <Typography variant="h5" color="textPrimary">30</Typography>

                        </ListItemAvatar>
                        <Divider orientation="vertical" style={{ height: "90px", margin: 0 }} />
                        <ListItemAvatar className="text-center" style={{ width: "30%", }}>

                            <span className="svg-icon svg-icon-white svg-icon-2x">{/*begin::Svg Icon | path:/var/www/preview.keenthemes.com/keen/releases/2021-02-02-111420/theme/demo1/dist/../src/media/svg/icons/General/Heart.svg*/}<svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24" />
                                    <path d="M16.5,4.5 C14.8905,4.5 13.00825,6.32463215 12,7.5 C10.99175,6.32463215 9.1095,4.5 7.5,4.5 C4.651,4.5 3,6.72217984 3,9.55040872 C3,12.6834696 6,16 12,19.5 C18,16 21,12.75 21,9.75 C21,6.92177112 19.349,4.5 16.5,4.5 Z" fill="#000000" fillRule="nonzero" />
                                </g>
                            </svg>{/*end::Svg Icon*/}</span>


                            <Typography variant="h5" color="textPrimary">2000</Typography>
                        </ListItemAvatar>

                    </ListItem>
                </List>
            </CardContent>
        </Card>
    )
}

export default ProfileStats