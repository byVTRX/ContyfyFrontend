
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom"
import CreatePost from "./CreatePostComponent";
import logo from "../Logo.png"
import algoliasearch from 'algoliasearch';

import $ from "jquery";

import postIcon from "./icon.png"



import { Avatar, Button, Card, CardActions, CardContent, CardHeader, ClickAwayListener, IconButton, makeStyles, Modal, Typography, Divider } from "@material-ui/core"



const useStyles = makeStyles({
  icon: {
    "-webkit-text-fill-color": "white",
    "-webkit-text-stroke-width": "1px",
    "-webkit-text-stroke-color": "black",
  },
  header: {
    display: "flex",
    position: "fixed",
    justifyContent: "space-between",
    flexDirection: "row",
    maxHeight: "70px",
    width: "100%",
    paddingLeft: "10%",
    paddingRight: "10%",
    paddingTop: "10px",
    zIndex: "100",
    background: "#16171a",
    alignItems: "center",
    borderBottom: "1px solid white"
  }, 
  searchItem:{
    background:"black",
    backgroundOpacity:"50%",
    borderRadius:"20px",
    "&:hover":{
      background: "linear-gradient(90deg, rgba(255,146,1,1) 0%, rgba(251,34,34,1) 81%);"
    }
  }
})
const Header = ({ user }) => {
  const history = useHistory();
  const [name, setName] = useState("")
  const [postWindow, showPostWindow] = useState(false)
  const [createPost, setCreatePost] = useState(false);
  const classes = useStyles();
  const [results, setResults] = useState([]);
  const [searchOpen, setSearchOpen] = useState(false)

  const client = algoliasearch('Z60K639WB7', '19d3fcd62b038f92d5d7a762d4dab2c0');
  const index = client.initIndex('Users');

  const searchUsers = async (string) => {
    index.search(string, {
      attributesToRetrieve: ['displayName', 'username', 'profilepicture'],
      hitsPerPage: 10,
    }).then(({ hits }) => {
      setResults(hits)
    });
  }

  const openSearch = () => {
    setSearchOpen(true);
  }

  const closeSearch = () => {
    setSearchOpen(false);
  }

  useEffect(() => {

    const runAsync = async () => {

      try {
        setName(await getName());

        console.log("Name was set to" + name);
      } catch (error) {
        throw error;
      }
    }

    runAsync();

  }, [])




  return (
    <div className={classes.header}>
      {/** BEGIN:LEFT SIDE */}
      <div>
        <img src={logo} width="50px" heigth="50px" style={{ cursor: "pointer" }} onClick={() => {
          history.push('/')
        }} />
      </div>

      {/** END:LEFT SIDE */}
      {/** BEGIN:CENTER SIDE */}
      {user.verifiedStatus >= 2 && <div style={{ position: "absolute", left: "49%" }}>
        <a href="/create-post"
        ><img src={postIcon} height="40px" width="40px" alt="POST" style={{ borderRadius: "5px", border: "0.5px solid black" }} /></a>
      </div>}

      {/** END:CENTER SIDE */}
      {/** BEGIN:RIGHT SIDE */}

      <form style={{ marginRight: "-5%" }}>
        <div className="form-group" >

          <div className="input-group" style={{ marginTop: "8px", }}>
            <div className="input-group-prepend" ><span className="input-group-text" id="search_bar_container" style={{ background: "transparent", borderRadius: "20px", }}><span className="svg-icon  svg-icon-2x svg-icon-white" >{/*begin::Svg Icon | path:/var/www/preview.keenthemes.com/keen/releases/2021-02-02-111420/theme/demo1/dist/../src/media/svg/icons/General/Search.svg*/}<svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" >
              <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                <rect x={0} y={0} width={24} height={24} />
                <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="white" fillRule="nonzero" opacity="0.3" />
                <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="white" fillRule="nonzero" />
              </g>
            </svg>{/*end::Svg Icon*/}</span>

              <input type="text" className="form-control" id="search_bar" onChange={(e) => searchUsers(e.target.value)} onFocus={() => {
                openSearch();
                document.getElementById('search_bar_container').style = "border-image-source:linear-gradient(90deg, rgba(255,146,1,1) 0%, rgba(251,34,34,1) 81%);border-width: 2px;border-image-slice: 1;background: transparent;"
              }} onBlur={() => {
                document.getElementById('search_bar_container').style = "background:transparent;border-radius:20px;"
                //closeSearch()
              }} placeholder="Search Contyfy..." />
            </span></div>
              
          </div>
          
        </div>
        {searchOpen && <SearchListComponent results={results} classes={classes} history={history}/>}
      </form>
      

      {/** END:RIGHT SIDE */}


    </div>
  )


}




async function checkLogin() {
  return null;//CHECK IF USER IS LOGGED IN
}

async function getName() {
  try {
    const fetch = null;//GET USER INFO
    const userAttributes = fetch.attributes;
    const name = userAttributes.given_name;

    return name
  } catch (error) {
    console.error(error)
    return "No Name found";
  }

}


function SearchListComponent({ results, classes,history }) {
  return (<>
    <ul style={{listStyle:"none", height:"100%",  width:"200px", position:"absolute", marginLeft:"-50px"}}>
      {results.map((result) => {
        return (<>
        <SearchItemComponent result={result} history={history} classes={classes} />
        <Divider/>
        </>)
      })}
    </ul>
  </>)
}

function SearchItemComponent({ result, classes,history }) {
  return (<li style={{ display: 'inline-flex', width:"270px", alignItems:"center",cursor: "pointer"}} onClick={(e) => 
  {
    e.preventDefault();
    history.push('/profile/' + result.username)
  }} className={classes.searchItem}>
    <Avatar src={result.profilepicture} />
    <div style={{ display: 'flex', flexDirection: 'column', marginLeft:"10px" }}>
      <p style={{ color: "white", fontSize: "12px", margin:"0" }}>{result.displayName}</p>
      <p style={{ color: "gray", fontSize: "8px", margin:"0" }}>@{result.username}</p>
    </div>
  </li>)
}

export default Header;