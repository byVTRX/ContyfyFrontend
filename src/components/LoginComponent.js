import React, { Component } from "react";
import { useHistory } from "react-router-dom"

import logo from "../Logo.png"

import $ from "jquery"



import firebase from "firebase"
import "firebase/auth"
import { Divider, Hidden, makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    form: {
        background: "transparent",
        border:"1px solid white",
        padding: "30px",
        borderRadius: "20px",
        display:"flex",
        justifyContent:"center",
        alignItems:"center",
        [theme.breakpoints.up('md')]: {
            width: "40%"
        },
        [theme.breakpoints.down('md')]: {
            maxHeight: "90%",
            height: "100%"
        }

    },
    root: {
        justifyContent: "center",
        alignItems: "center",
        display: "flex",
        maxHeight: "100vh",
        maxWidth:"100vw"
    },
    inner:{
        [theme.breakpoints.up('md')]:{
            padding:"20px",
            borderRight:"1px solid white",

        },

        [theme.breakpoints.down('md')]:{
            maxHeight:"100%",
            alignItems:"center",
            display:"flex",
            flexDirection:"column",
        }
        
    },
    logo:{
        width:"90%",
        display:"flex",
        alignItems:"center",
        justifyContent:"center",
        flexDirection:"column",
        color:"black",
    }
}))

export default function Login() {
    const history = useHistory();
    const classes = useStyles();
    return (<div className={classes.root}>
        <form onSubmit={async (event) => {
            var loggedIn = false;
            event.preventDefault()
            loggedIn = await signIn($('#username').val(), $('#password').val())
            if (loggedIn) {
                history.push('/')
            }

        }} className={classes.form}>
            <div className={classes.inner}>
                <h3>Log in</h3>

                <div className="form-group">
                    <label style={{color:"white"}}>Email</label>
                    <input type="text" className="form-control" placeholder="Enter email" id="username" style={{ color: "black" }} />
                </div>

                <div className="form-group">
                    <label style={{color:"white"}}>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" id="password" style={{ color: "black" }} />
                </div>



                <button type="submit" className="btn btn-primary btn-lg btn-block">Sign in</button>
                <p className="forgot-password text-right">
                    Forgot <a href="#">password?</a>
                </p>
                <p className="forgot-password text-right">
                    Don't have an account? <a href="/sign-up">Register</a>
                </p>
            </div>
            <Hidden mdDown>
            
            <div className={classes.logo}>
                <img src={logo} widht="300px" height="300px" alt="CONTYFY"/>
                <h3>Contyfy</h3>
            </div>
            </Hidden>
            

        </form>
    </div>);

}

async function signIn(username, password) {
    
    return await firebase.auth().signInWithEmailAndPassword(username, password).then(() => {
        return !firebase.auth().currentUser.emailVerified && firebase.auth().currentUser.sendEmailVerification();
    });

}

