import React, { useEffect, useState } from 'react';

import Plyr from 'plyr-react'
import 'plyr-react/dist/plyr.css'
import { makeStyles } from '@material-ui/core/styles';
import {
  Button,
  List,
  ListItem,
  Divider,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Typography,
  CircularProgress,
  Card,
  CardContent,
  CardHeader,
  CardActions,
  IconButton,
  CardMedia,
  Collapse,
  Grid,
  TextField,
  MenuItem,
  Menu,
  Select,
  InputLabel,
  FormControl,
} from '@material-ui/core';


import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


import MonetizationOnRoundedIcon from '@material-ui/icons/MonetizationOnRounded';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import $ from "jquery";

import { CommentItem } from "./CommentListComponent"

import firebase from "firebase"
import Picker from "emoji-picker-react"

import _ from "lodash";

import alertify from "alertifyjs";
import 'alertifyjs/build/css/alertify.css';

import { createPopper } from "@popperjs/core"

import ReactHlsPlayer from "react-hls-player";

import EmojiEmotionsIcon from '@material-ui/icons/EmojiEmotions';

import { useHistory } from 'react-router';
import moment from 'moment';
import "./main.css"

import verified from "./assets/Verified.svg"



import videojs from "video.js";
import "video.js/dist/video-js.css";


import CommentList from './CommentListComponent';
import MonetizationOnRounded from '@material-ui/icons/MonetizationOnRounded';

const useStyles = makeStyles(theme => ({
  listRoot: {
    width: '100%',
    wordBreak: 'break-all',
    overflow: 'none',
    listStyleType: "none",


  },
  alignCenter: {
    textAlign: 'center',
  },
  loader: {
    textAlign: 'center',
    paddingTop: 20,
  },
  maxWidth: {
    width: '100%',
  },
  listHeader: {
    position: 'sticky',
    top: 0,
    zIndex: 1200,
    backgroundColor: '#15202B',
    borderBottom: '1px solid #37444C',
    listStyleType: "none"
  },
  clickable: {
    cursor: 'pointer',


    display: "flex",
    width: "45%"
  },
  root: {
    width: "100%",
    minWidth: 345,
    borderRadius: 0,
    listStyleType: "none",

    background: "50%"
  },
  media: {
    height: "500px",
    maxWidth: "95%",
    margin: "10px auto",
    marginTop: "0px",
    borderRadius: "10px",
    alignSelf: "center",
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: "white",
    width: theme.spacing(7),
    height: theme.spacing(7),
    marginLeft: "10px"
  },
  divider: {
    borderRight: "1px solid white",
    width: "1px"
  },
  selector: {

    background: "transparent",
    borderBottom: "1px solid",
    height: "55px",
    marginTop: "-50px",
    alignItems: "center"
  },
  selectorText: {
    "&:hover": {
      textDecorationColor: "linear-gradient(90deg, rgba(255,146,1,1) 0%, rgba(251,34,34,1) 81%);"
    }
  },
  clickable: {
    "&:hover": {
      background: "white",
      "-webkit-background-clip": "text",
      "-webkit-text-fill-color": "transparent",
    },
    background: "white",
    "-webkit-background-clip": "text",
    "-webkit-text-fill-color": "transparent",


  },
  grid: {
    background: "#282a30",

    "&:hover": {
      background: "linear-gradient(90deg, rgba(255,146,1,1) 0%, rgba(251,34,34,1) 81%);"
    }
  },
  likeButton: {
    color: "black"
  },
  commentForm: {
    display: "inline-flex",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "50px",
    padding: "20px 10px",
    textDecoration: "white"
  },
  paper: {

    background: "#424242"

  }
}));

export default function PostList({ isLoading, posts, getAdditionalPosts, listHeaderTitle, listHeaderTitleButton, showCategories, user }) {

  const classes = useStyles();
  return (
    <div className={classes.listRoot}>
      {isLoading ?
        <div className={classes.loader}>
          <CircularProgress size={25} />
        </div>
        :
        <ul style={{ listStyleType: "none", margin: "0 auto", padding: 0 }}>


          {
            showCategories ? <li className="flex-start text-center" style={{ position: "relative", }}>
              <Card width="100%" className={classes.selector} id="selector">
                <CardContent className="text-center" style={{ width: "100%", display: "flex", margin: 0, padding: 0, height: "100%" }}>
                  <Grid container spacing={3} style={{ width: "100%", height: "100%", margin: 0, padding: 0 }} >
                    <Grid item xs={6} style={{ borderRight: "1px solid", padding: 0, margin: 0, height: "100%" }} className={classes.grid}>
                      <Button className={classes.clickable} style={{ padding: 0, width: "100%", height: "100%" }}>
                        <Typography variant="subtitle1" style={{ fontSize: "25px" }}>Subscriptions</Typography>
                      </Button>
                    </Grid>


                    <Grid item xs={6} className={classes.grid} style={{ margin: 0, padding: 0, height: "100%" }}>
                      <Button className={classes.clickable} style={{ padding: 0, width: "100%", height: "100%" }}>
                        <Typography variant="subtitle1" style={{ fontSize: "25px" }}>Purchased Content</Typography>
                      </Button>
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>

            </li>
              :
              <div></div>
          }



          {posts.map(post => (
            <span key={post.id}>
              <PostItem postObj={post} postId={post.id} user={user} />
              <Divider />
            </span>
          ))}



        </ul>
      }
    </div>
  )
}

function PostItem({ postObj, postId, user }) {
  const classes = useStyles();
  const history = useHistory();
  const now = moment();
  const [media, setMedia] = useState("");
  const [creatorData, setCratorData] = useState({ displayname: "", username: "", profilepicture: "", verifiedstatus: "" });
  const [liked, setLiked] = useState(null);
  var post = postObj.post;
  const [commentVal, setCommentVal] = useState('');
  const [comments, setComments] = useState([]);
  const [commentsOpen, setCommentsOpen] = useState(false);

  
  const [tipData, setTipData] = useState({ amount: 1000 });
  const [emojiPicker, setEmojiPicker] = useState(false);
  const [confirmWindowOpen, setConfirmWindowOpen] = useState(false)
  const [tipWindowOpen, setTipWindowOpen] = useState(false)
  const [paymentMethod, setPaymentMethod] = useState(null)

  const [menuAnchor, setMenuAnchor] = useState(null);

  const [likeDisabled, setLikeDisabled] = useState(false);



  useEffect(() => {




    async function fetchPost() {
      var getUser = firebase.functions().httpsCallable('getUser')
      var fetchResult = await getUser({ username: post.username })
      setCratorData(fetchResult.data.user)


      var getPostComments = firebase.functions().httpsCallable('getPostComments');
      var postComments = await getPostComments({ postId, creatorId: post.username })


      const userLikeObj = null;//GET LIKES BY POST ID AND USER ID
      console.log(userLikeObj)


      const likesObj = post.likes;//GET ALL LIKES FROM POST

      const commentsObj = postComments.data.status === 200 ? postComments.data.comments : null;//GET ALL COMMENTS FROM POST


      setComments(commentsObj)





      console.log(likesObj)
      if (likesObj.length === 0) {
        setLiked(null)
      } else {
        var likedBool = false;
        await likesObj.forEach(like => {
          if (like.username === user.username) {
            likedBool = true;
          }
        })
        setLiked(likedBool)
      }

















      async function fetchData() {







        if (post.postType === "image" || post.postType === "video") {
          setMedia(post.media)
        } else {
          setMedia("https://dummyimage.com/1920x1080/000/f0f0f0&text=You+have+to+subscribe+to+view+this+post")
        }




      }

      if (post.media && (post.postType === "image" || post.postType === "video")) {

        fetchData();

      }
    }

    fetchPost()
  }, [])

  const requestTipPayment = async (amount) => {
    const req = await firebase.functions().httpsCallable('requestTipPayment');
    const res = await req({ creatorId: post.username, amount: amount * 100, customerId: user.stripeId, postId });
    if (res.data.status === 200) {
      setTipData(res.data.tipData);
      openConfirmWindow();
    }

  }

  const openConfirmWindow = () => {
    setConfirmWindowOpen(true);
  }

  const openTipWindow = () => {
    setTipWindowOpen(true);
  }

  const closeTipWindow = () => {
    setTipWindowOpen(false);
  }

  const closeConfirmWindow = () => {
    setConfirmWindowOpen(false)
  }

  const confirmTip = async () => {
    const req = await firebase.functions().httpsCallable('confirmTipPayment');
    const res = await req({creatorId: post.username, paymentId: tipData.paymentId, email: user.email, paymentMethod,postId });
    if (res.data.status === 200) {

      alertify.success("You successfully donated $" + tipData.amount / 100 + " to this post.")
    }
  }

  const sendComment = async (commentText) => {

    console.log("SENDING COMMENT " + commentText)




    try {
      console.log("SENDING REQUEST")
      const createComment = await firebase.functions().httpsCallable("createComment");//CREATING COMMENT
      const res = await createComment({ postId, creatorId: post.username, content: commentText })
      console.log(res)

      if (res.data.status !== 200) {

        alert('Failed to post your comment. Please try again later')


      } else {
        const comment = res.data.comment;
        $('#commentList').prepend(<CommentItem user={user} post={post} commentId={comment.id} comment={comment.comment} />)
        return res
      }
    } catch (error) {
      console.error(error)
      alert('Failed to post your comment. Please try again later')
      return error
    }


  }

  const openMenu = (event) => {
    setMenuAnchor(event.currentTarget)
  }

  const handleCloseMenu = () => {
    setMenuAnchor(null);
  }

  const calcTimestampDiff = (timestamp) => {
    const time = new Date(timestamp * 1000).getTime();
    const scales = ['years', 'months', 'weeks', 'days', 'hours', 'minutes', 'seconds'];

    for (let i = 0; i < scales.length; i++) {
      const scale = scales[i];
      const diff = moment(now).diff(time, scale);
      if (diff > 0) return diff + scale.charAt(0)
    }

    return 0 + scales[scales.length - 1].charAt(0)
  }


  const toggleLike = async (postId) => {

    const userId = user.id

    console.log(liked)
    if (liked !== null) {
      console.log("post is liked")
      setLikeDisabled(true)
      try {
        const unLikePost = firebase.functions().httpsCallable('unLikePost');
        const unLiked = await unLikePost({ postId, liked, creatorId: post.username });

        if (unLiked.data.status === 200) {
          setLiked(null)
          postObj.post.likes = postObj.post.likes.filter((value, index, s) => {
            return value.username != userId;
          })
          setLikeDisabled(false);
          return unLiked;
        } else {

          setLikeDisabled(false)
          return null;
        }
      } catch (error) {
        setLikeDisabled(false);
        return null;
      }










    } else {
      console.log("Post is not liked")
      setLikeDisabled(true)


      try {
        console.log(post.username + " " + post.id)
        const likePost = firebase.functions().httpsCallable('likePost');
        const newLike = await likePost({ postId, postUser: post.username })



        if (newLike.data.status === 200) {
          post.likes.push(newLike)

          setLiked(newLike)
          setLikeDisabled(false);
          return newLike
        } else {

          setLiked(null);
          setLikeDisabled(false)
          return null;
        }

      } catch (error) {
        setLiked(null)
        setLikeDisabled(false)
        return null
      }



    }


  }

  const videoSrc = {
    type: 'video',
    sources: [
      {
        src: post.media
      }
    ]
  };

  return (<React.Fragment>
    <li className="flex-start" key={postId} >


      {/** 
              <Card className={classes.root}>
  
              <CardContent>
      <Typography className={classes.title}>
      <ListItemAvatar>
                <div className={classes.clickable} onClick={() => history.push('/' + post.owner)}>
                  <Avatar alt={post.displayName} src='/' />
                    <h3>
                      {post.displayName}
                      <small> @{post.owner}</small>
                    </h3>
                </div>
                
              </ListItemAvatar>
              
      </Typography>
      
    
    <div className="card-toolbar">
      <small className="card-label" style={{marginRight:"10px"}}>{ ' ' + calcTimestampDiff(post.createdAt) + ' ' + String.fromCharCode(183)}</small>
      <a href="#" className="btn btn-sm btn-success font-weight-bold text-center align-items-center justify-content-center" style={{height:"30px", width:"30px"}}>
        <i className="ki ki-more-hor"/>
      </a>
    </div>
    
  
    <p className="card-label">
    {post.title}
    </p>
    
  </CardContent>
  <CardActions>
            <a>
            <span className="far fa-heart" ></span>
            <small > 420</small>
            </a>
            <a>
            <span className="fas fa-dollar-sign" ></span>
            
            </a>
            <a>
            <span className="far fa-comment-alt" ></span>
            <small > 420</small>
            </a>
              
             
  </CardActions>
</Card>
*/}

      <Card className={classes.root} style={{ marginBottom: "0px" }} elevation={3}>
        <CardHeader
          avatar={
            <Avatar style={{ cursor: "pointer" }} aria-label="recipe" src={creatorData.profilepicture ? creatorData.profilepicture : null} className={classes.avatar} onClick={() => { history.push('/profile/' + post.username) }}>

            </Avatar>
          }
          action={
            <div style={{ marginRight: "10px", width: "100%", display: "inline-flex" }}>
              <Typography style={{ marginTop: "10px", color: "white" }}>{' ' + calcTimestampDiff(post.timestamp) + ' ' + String.fromCharCode(183)}</Typography>
              <IconButton aria-label="settings" aria-controls="menu" aria-haspopup="true" onClick={openMenu}>
                <MoreVertIcon />
              </IconButton>
              <Menu
                id="menu"
                anchorEl={menuAnchor}
                keepMounted
                open={Boolean(menuAnchor)}
                onClose={handleCloseMenu}
              >
                <MenuItem onClick={handleCloseMenu}>Report</MenuItem>
                <MenuItem onClick={handleCloseMenu}>Edit Post</MenuItem>
                <MenuItem onClick={handleCloseMenu}>Delete Post</MenuItem>
              </Menu>
            </div>

          }
          title={
            <Typography variant="h5" style={{ color: "white" }}>
              {creatorData.displayName} {creatorData.verifiedStatus == 2 && <img width="20px" height="20px" src={verified} />}
            </Typography>

          }
          subheader={<Typography style={{ color: "grey" }}>{"@" + creatorData.username}</Typography>}
          style={{ textAlign: "left" }}
        />
        {post.title ? <CardContent style={{ marginLeft: "26px", padding: 0 }}>
          <Typography variant="subtitle1" color="textPrimary" component="h2" style={{ color: "white", textAlign: "left" }}>
            {post.title}
          </Typography>
        </CardContent> : null}

        {post.media &&
          (post.postType === "image" ?
            <CardMedia
              className={classes.media}
              image={post.media}
              title={post.title}
            />

            :
            <CardMedia
              className={classes.media}
            >
              <Plyr

                source={videoSrc}
              >
              </Plyr>
            </CardMedia>






          )

        }



        <CardActions disableSpacing style={{ marginLeft: "10px" }}>
          <IconButton aria-label="add to favorites" onClick={async (e) => {
            if (!likeDisabled) {

              toggleLike(postId)
            }
          }}>

            {liked ? <span className="fas fa-heart" style={{ color: "red" }}></span> : <span className="far fa-heart"></span>}
            <small style={{ marginLeft: "10px" }}>{post.likes.length}</small>


          </IconButton>
          {post.allowTips &&

            <IconButton aria-label="share" onClick={(e) => {
              e.preventDefault()
              openTipWindow();
              /*
              alertify.defaults.transition = "slide";
              alertify.defaults.theme.ok = "btn btn-primary";
              alertify.defaults.theme.cancel = "btn btn-danger";
              alertify.defaults.theme.input = "form-control";
              alertify.prompt("Post donation", "How much do you want to tip for this post?", "10", (evt, value) => { requestTipPayment(value) }, () => { alertify.error("You were not charged") }).set("type", "number").set('labels', { ok: "Tip" })
            */}}>
              <span className="fas fa-dollar-sign" ></span>
              
              
            </IconButton>
          }
<Dialog open={tipWindowOpen} onClose={closeTipWindow} classes={{
                paper: classes.paper,
              }}>
                <DialogTitle>
                  Post donation
                </DialogTitle>
                <DialogContent>
                  <DialogContentText>
                    How much do you want to tip for this post?
                  </DialogContentText>
                  <TextField
                    autoFocus
                    margin="dense"
                    id="amount"
                    label="Tip Amount"
                    type="number"
                    fullWidth
                  />
                </DialogContent>
                <DialogActions>
                  <Button onClick={async (e) => {
                    e.preventDefault()
                  closeTipWindow();
                  console.log(tipWindowOpen)
                   requestTipPayment($('#amount').val());
                  }} color="primary">
                    Tip
                  </Button>

                  <Button onClick={async (e) => {
                    e.preventDefault()
                    closeTipWindow();
                    console.log(tipWindowOpen)
                    }} color="primary">
                    Cancel
                  </Button>
                </DialogActions>
              </Dialog>


              <Dialog open={confirmWindowOpen} onClose={closeConfirmWindow} classes={{
                paper: classes.paper,
              }}>
                <DialogTitle>
                  Confirm your purchase
                </DialogTitle>
                <DialogContent style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                  <MonetizationOnRounded color="primary" style={{ fontSize: "100px" }} />
                  <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">Select payment method</InputLabel>
                    <Select
                      variant="outlined"
                      labelId="demo-simple-select-label"
                      id="paymentMethodSelection"

                      value={paymentMethod}
                      onChange={(e) => {
                        e.preventDefault()
                        setPaymentMethod(e.target.value)
                        console.log(paymentMethod)
                      }}
                    >
                      {tipData.paymentMethods && tipData.paymentMethods.map((paymentMethod) => {
                        return <MenuItem value={paymentMethod.id}>{paymentMethod.card.brand.toUpperCase() + " - " + paymentMethod.card.last4 + " - " + paymentMethod.card.exp_month + "/" + paymentMethod.card.exp_year}</MenuItem>
                      })}


                    </Select>
                  </FormControl>
                  <p style={{ color: "white" }}>There will be <b>${tipData.amount / 100}</b> charged to your chosen payment method.</p>
                  <small>A refund of this donation will not be granted</small>
                </DialogContent>
                <DialogActions>
                  <Button onClick={() => {
                    closeConfirmWindow();
                    confirmTip();
                  }} color="primary">
                    Confirm
                  </Button>
                  <Button onClick={closeConfirmWindow} color="primary">
                    Cancel
                  </Button>
                </DialogActions>
              </Dialog>
          {post.allowComments &&
            <IconButton
              onClick={(e) => {

                setCommentsOpen(!commentsOpen)
              }}
              aria-expanded={true}
              aria-label="show more"
            >
              <span className="far fa-comment-alt" ></span>
              <small style={{ marginLeft: "10px" }}>{comments.length}</small>
            </IconButton>
          }

        </CardActions>
        <Collapse in={commentsOpen} timeout="auto">
          <CardContent style={{ margin: 0, padding: 0 }}>
            <CommentList comments={comments} user={user} isLoading={false} post={post} postId={postId} style={{ width: "100%", margin: "0", padding: "0" }} />


          </CardContent>
        </Collapse>
        <Divider />
        <div style={{ maxHeight: "60px" }}>

          {post.allowComments &&
            <form className={classes.commentForm}>





              <EmojiEmotionsIcon id="emojiIcon" style={{ cursor: "pointer", margin: "10px" }} onClick={(e) => {
                e.preventDefault();
                setEmojiPicker(!emojiPicker)

              }} />

              <textarea rows={1} id="commentText" value={commentVal} label="Comment" onChange={(e) => {
                setCommentVal(e.target.value)

              }} placeholder="Comment on this post..." required style={{ width: "90%", height: "30px", color: "white", fontSize: "15px", background: "transparent", border: "none", resize: "none" }} />
              <IconButton color="white" onClick={(e) => {
                e.preventDefault()

                sendComment($('#commentText').val());
              }}>
                <span className="svg-icon svg-icon-2x"><svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                    <rect x={0} y={0} width={24} height={24} />
                    <path d="M3,13.5 L19,12 L3,10.5 L3,3.7732928 C3,3.70255344 3.01501031,3.63261921 3.04403925,3.56811047 C3.15735832,3.3162903 3.45336217,3.20401298 3.70518234,3.31733205 L21.9867539,11.5440392 C22.098181,11.5941815 22.1873901,11.6833905 22.2375323,11.7948177 C22.3508514,12.0466378 22.2385741,12.3426417 21.9867539,12.4559608 L3.70518234,20.6826679 C3.64067359,20.7116969 3.57073936,20.7267072 3.5,20.7267072 C3.22385763,20.7267072 3,20.5028496 3,20.2267072 L3,13.5 Z" fill="#000000" />
                  </g>
                </svg></span>
              </IconButton>
            </form>
          }

        </div>
        {emojiPicker &&
          <div style={{ marginLeft: "20px" }}>
            <Picker id="emojiPicker" onEmojiClick={(e, emojiObj) => {
              e.preventDefault()
              setCommentVal(commentVal + emojiObj.emoji)
            }} />
          </div>

        }


      </Card>





      {/*
      <ListItemText
        primary={
          <React.Fragment>
            {post.displayName}
            <Typography
              color='textSecondary'
              display='inline'
            >
              {' ' + String.fromCharCode(183) + ' ' + calcTimestampDiff(post.createdAt)}
            </Typography>
          </React.Fragment>
        }
        secondary={
          <React.Fragment>
            <Typography
            color='textPrimary'
            style={{marginBottom:"10px"}}
          >
            {post.title}
            {displayMediaPost(post)}
            
          </Typography>
          <Typography color='textSecondary'>
            <Divider />
            <div className="row justify-content-start align-items-center" >
              <span className="far fa-heart" style={{margin:"5px"}}></span>
              <small className="interactions">420</small>
              <span className="fas fa-dollar-sign" style={{margin:"5px"}}></span>
              <small className="interactions">$69</small>
              <span className="fa fa-comment-alt" style={{margin:"5px"}}></span>
              <small className="interactions">23</small>
            </div>
          </Typography>
          </React.Fragment>
          
          
        }
      /> */}
    </li>
  </React.Fragment>

  )
}





{/**

background:"linear-gradient(90deg, rgba(255,146,1,1) 0%, rgba(251,34,34,1) 81%);",

*/}

class VideoPlayer extends React.Component {
  componentDidMount() {
    this.player = videojs(this.videoNode, this.props);
  }

  componentWillUnmount() {
    if (this.player) {
      this.player.dispose();
    }
  }

  render() {
    return (
      <div>
        <div data-vjs-player>
          <video
            ref={(node) => {
              this.videoNode = node;
            }}
            className="video-js"
          />
        </div>
      </div>
    );
  }
}

const videoJsOptions = {
  autoplay: true,
  controls: true,
  sources: [
    {
      src: null,
    },
  ],
};

const videoOnDemandJsOptions = (fileName) => {
  return ({
    autoplay: true,
    controls: true,
    sources: [
      {
        src: "https://${awsvideoconfig.awsOutputVideo}/public/" + fileName,
      },
    ],
  })
}




