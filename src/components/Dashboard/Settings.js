import React, { useEffect, useState } from "react";
import EditIcon from '@material-ui/icons/Edit';

import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';

import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';

import StarIcon from '@material-ui/icons/Star';

import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import ContactsIcon from '@material-ui/icons/Contacts';

import SaveIcon from '@material-ui/icons/Save';

import HelpIcon from '@material-ui/icons/Help';

import crypto from "crypto";
import alertify from "alertifyjs";

import firebase from "firebase/app";
import "firebase/functions"
import "firebase/storage"

import $ from "jquery";

import { Avatar, Badge, CircularProgress, Container, Fab, FormControl, IconButton, InputAdornment, InputLabel, makeStyles, MenuItem, Select, TextField } from "@material-ui/core";

const useStyles = makeStyles((theme => ({
    root: {
        height: "100vh",
        width: "100%",
        justifySelf: "center",
        alignItems: "center",
        justifyContent: "center",
        display:"block",
        
    },
    mediaSettings: {
        width: "100%",
        height: "20%",
        borderBottom: "1px solid white",
        display: "inline-flex",
        alignContent: "center",
        justifyContent: "center",
        "&:hover": {
            opacity: "0.5",
            cursor: "pointer"
        }
    },
    publicSettings: {
        [theme.breakpoints.down('md')]: {
            display: "flex",
            flexDirection: "column",
        },
        [theme.breakpoints.up('md')]: {
            display: "grid",
            gridTemplateRows: "70% 30%",
            gridTemplateColumns: "25% 25% 25% 25%",
        },
        height: "30%",
        borderBottom: "1px solid white",

        padding: "50px"
    },
    avatar: {
        width: theme.spacing(12),
        height: theme.spacing(12),
        justifySelf: "flex-start"
    },
    profilePic: {
        gridColumnStart: "1",
        gridColumnEnd: "2",
        gridRowStart: "1",
        gridRowEnd: "2",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },
    names: {
        gridColumnStart: "2",
        gridColumnEnd: "5",
        gridRowStart: "1",
        gridRowEnd: "2",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    socials: {
        gridColumnStart: "1",
        gridColumnEnd: "5",
        gridRowStart: "2",
        gridRowEnd: "3",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    contactSettings: {
        gridTemplateColumns: "25% 25% 25% 25%",
        gridTemplateRows: "50% 50% ",
        alignItems: "center",
        justifyContent: "center",
        display: "grid",
        gridGap: "20px",
        height: "30%",
        padding: "50px",
        borderBottom: "1px solid white",
    },
    creatorSettings: {
        gridTemplateColumns: "25% 25% 25% 25%",
        gridTemplateRows: "50% 50% ",
        alignItems: "center",
        justifyContent: "center",
        display: "grid",
        gridGap: "20px",
        height: "30%",
        padding: "50px",
        borderBottom: "1px solid white",
    }
})))

export default function Settings({ settings, user }) {
    const classes = useStyles();
    const [loading, setLoading] = useState(false);
    const [pictureUpload, setPictureUpload] = useState(false)
    const [bannerUpload, setBannerUpload] = useState(false)

    const setupStripe = async () => {
        if (user.verifiedStatus < 2) {
            setLoading(true)
            var requestCreator = firebase.functions().httpsCallable('approveCreator');
            const res = await requestCreator();
            const data = res.data;
            if (data.status == 200) {
                
                window.location.href = data.url;
                setLoading(false)
            } else {
                alert("Something went wrong while trying to onboard the user: " + JSON.stringify(data))
            }
        } else {
            alert('You are already registered as a creator')
        }

    }


    const updateSettings = async () => {
        setLoading(true)

        var profilepictureURL = settings.profilepicture || "";
        var bannerURL = settings.banner || "";
        
        var newSettings = {
        }

        

        const putPicture = async () => {
            var id = crypto.randomBytes(30).toString('hex');
            const upload = firebase.storage().ref("images/" + settings.username + "/" + `${id}.` + getExtension(pictureUpload.name)).put(pictureUpload)
            return upload.then((snapshot) => {
                return firebase.storage().ref("images/" + settings.username).child(`${id}.${getExtension(pictureUpload.name)}`).getDownloadURL().then((fileUrl) => {
                
                    return fileUrl;
      
                  })
            })
           
          }

          if(pictureUpload){
              profilepictureURL = await putPicture();
          }

       

            const putBanner = async () => {
                var id = crypto.randomBytes(30).toString('hex');
                const upload = firebase.storage().ref("images/" + settings.username + "/" + `${id}.` + getExtension(bannerUpload.name)).put(bannerUpload)
                return upload.then((snapshot) => {
                    return firebase.storage().ref("images/" + settings.username).child(`${id}.${getExtension(bannerUpload.name)}`).getDownloadURL().then((fileUrl) => {
                   
                        return fileUrl;
           
                       })
                })
                
              }

            if(bannerUpload){
                  bannerURL = await putBanner()
            }
        
              
              
        

        if (user.verifiedStatus >= 2) {
            newSettings = {
            email: $('#email').val(),
            phoneNumber: $('#phonenumber').val(),
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val(),
            profilepicture: profilepictureURL,
            displayName: $('#displayName').val(),
            //TODO: BIRTHDATE
            //TODO: NOTIFICATIONSETTINGS
            banner:bannerURL,
            instagram: $('#instagram').val(),
            twitter: $('#twitter').val(),
            creatorSettings: {
                payoutSchedule: settings.creatorSettings.payoutSchedule,
                bio: $('#bio').val(),
                messagesFrom: settings.creatorSettings.messagesFrom,
                productID:user.creatorSettings.productID,
                stripeID:user.creatorSettings.stripeID,
                subscriptionpriceID:user.creatorSettings.subscriptionpriceID,
            }
            //TODO: MORE SOCIAL PROVIDERS
            }
            
        } else {
            newSettings = {
                email: $('#email').val(),
            phoneNumber: $('#phonenumber').val(),
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val(),
            profilepicture: profilepictureURL,
            displayName: $('#displayName').val(),
            //TODO: BIRTHDATE
            //TODO: NOTIFICATIONSETTINGS
            banner:bannerURL,
            instagram: $('#instagram').val(),
            twitter: $('#twitter').val(),
            //TODO: MORE SOCIAL PROVIDERS
            }
        }

        const updateRemoteSettings = firebase.functions().httpsCallable('updateSettings');
        const res = await updateRemoteSettings(newSettings);
        setLoading(false)

    }


    if (settings === "error" || settings === null) {
        return (<>
            <h1>An Error occured while loading settings for this account</h1>
        </>)
    } else {
        return (<>
            <div className={classes.root}>

                <div className={classes.mediaSettings}>

                    <img style={{ objectFit: "cover" }} id="banner" alt="Click here to upload a banner" src={settings.banner ? settings.banner : "https://c.wallhere.com/photos/10/ec/simple_background-118406.jpg!d"} width="100%" height="100%" onClick={() => document.getElementById('bannerinput').click()} />
                    <input type="file" id="bannerinput" style={{ display: "none" }} accept=".png,.jpg,.gif,.bmp" onChange={(event) => {
                        event.preventDefault()
                        addBanner(event.target.files[0], setBannerUpload)


                    }} />
                </div>
                <div className={classes.publicSettings}>
                    <div className={classes.profilePic}>
                        <Badge overlap="circle" badgeContent={<IconButton onClick={() => { document.getElementById('pictureinput').click() }}><EditIcon /></IconButton>}>
                            <Avatar className={classes.avatar} id="avatar" src={settings.profilepicture} />
                        </Badge>
                        <input type="file" id="pictureinput" accept=".png,.jpg,.gif,.bmp" style={{ display: "none" }} onChange={(e) => {
                            e.preventDefault();
                           addProfilePic(e.target.files[0], setPictureUpload)
                        }} />
                    </div>

                    <div className={classes.names}>
                        <TextField required id="displayName" variant="outlined" label="display name" defaultValue={settings.displayName} style={{ width: "49%" }} />
                        <div style={{ width: "10px" }} />
                        <TextField InputProps={{ readOnly: true }} variant="outlined" id="username" label="username(cannot be changed)" defaultValue={settings.username} style={{ width: "49%" }} />
                    </div>

                    <div className={classes.socials}>
                        <TextField id="twitter" placeholder="Enter Twitter URL" InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <TwitterIcon />
                                </InputAdornment>
                            )
                        }} variant="outlined" defaultValue={settings.twitter} style={{ width: "49%" }} />
                        <div style={{ width: "10px" }} />
                        <TextField variant="outlined" placeholder={"Enter Instagram URL"} InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <InstagramIcon />
                                </InputAdornment>
                            )
                        }} id="instagram" defaultValue={settings.instagram} style={{ width: "49%" }} />
                    </div>
                </div>

                <div className={classes.contactSettings}>
                    <TextField variant="outlined" defaultValue={settings.phoneNumber} style={{ gridColumnStart: "1", gridColumnEnd: "3", gridRow: "1" }} placeholder="Enter phonenumber" InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <PhoneIcon />
                            </InputAdornment>
                        )
                    }} id="phonenumber" />
                    <TextField variant="outlined" defaultValue={settings.email} style={{ gridColumnStart: "3", gridColumnEnd: "5", gridRow: "1" }} placeholder="Enter email" InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <EmailIcon />
                            </InputAdornment>
                        )
                    }} id="email" />
                    {user.verifiedStatus >0 ? 
                    <TextField disabled variant="outlined" defaultValue={settings.firstName} style={{ gridColumnStart: "1", gridColumnEnd: "3", gridRow: "2" }} placeholder="Enter first name" InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <PermIdentityIcon />
                            </InputAdornment>
                        )
                    }} id="firstName" /> : 
                    <TextField variant="outlined" defaultValue={settings.firstName} style={{ gridColumnStart: "1", gridColumnEnd: "3", gridRow: "2" }} placeholder="Enter first name" InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <PermIdentityIcon />
                            </InputAdornment>
                        )
                    }} id="firstName" />}


                    {user.verifiedStatus > 0 ? 
                    <TextField disabled variant="outlined" defaultValue={settings.lastName} style={{ gridColumnStart: "3", gridColumnEnd: "5", gridRow: "2" }} placeholder="Enter last name" InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <ContactsIcon />
                            </InputAdornment>
                        )
                    }} id="lastName" /> : 
                    <TextField variant="outlined" defaultValue={settings.lastName} style={{ gridColumnStart: "3", gridColumnEnd: "5", gridRow: "2" }} placeholder="Enter last name" InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <ContactsIcon />
                            </InputAdornment>
                        )
                    }} id="lastName" />}

                </div>
                {user.verifiedStatus >= 2 &&
                    <div className={classes.creatorSettings}>
                        <TextField disabled type="number" label="Subscription price" variant="outlined" defaultValue={settings.creatorSettings.subscriptionprice} style={{ gridColumnStart: "1", gridColumnEnd: "3", gridRow: "1" }} placeholder="Enter subscriptionprice" InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    $
                                </InputAdornment>
                            )
                        }} id="subscriptionprice" />
                        <FormControl style={{ gridColumnStart: "3", gridColumnEnd: "4", gridRow: "1"}}>
                            <InputLabel id="demo-simple-select-label">Payout schedule</InputLabel>
                            <Select
                                variant="outlined"
                                labelId="demo-simple-select-label"
                                id="payoutschedule"
                                value={settings.creatorSettings.payoutSchedule}
                                onChange={(e) => {
                                    e.preventDefault()
                                    settings.creatorSettings.payoutSchedule = e.target.value}}
                            >
                                <MenuItem value={"monthly"}>Monthly</MenuItem>
                                <MenuItem value={"weekly"}>Weekly</MenuItem>
                                <MenuItem value={"dayly"}>Dayly</MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl style={{ gridColumnStart: "4", gridColumnEnd: "5", gridRow: "1", margin:"10px" }}>
                            <InputLabel id="messageslabel">Messages From</InputLabel>
                            <Select
                                variant="outlined"
                                labelId="messageslabelid"
                                id="messagesFrom"
                                value={settings.creatorSettings.messagesFrom}
                                onChange={(e) => {settings.creatorSettings.messagesFrom = e.target.value}}
                            >
                                <MenuItem value={"everyone"}>Everyone</MenuItem>
                                <MenuItem value={"subscriber"}>Subscriber</MenuItem>
                                <MenuItem value={"none"}>Nobody</MenuItem>
                            </Select>
                        </FormControl>
                        <TextField variant="outlined" defaultValue={settings.creatorSettings.bio} style={{ gridColumnStart: "1", gridColumnEnd: "5", gridRow: "2" }} placeholder="Enter bio" InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <HelpIcon/>
                                </InputAdornment>
                            )
                        }} id="bio" />
                    </div>
                }

                {user.verifiedStatus < 2 && <Container className={`fluid btn btn-primary ${loading && `disabled`}`} onClick={!loading && setupStripe} style={{ margin: "20px", width: "auto", background: "linear-gradient(90deg, rgba(255,146,1,1) 0%, rgba(251,34,34,1) 81%)" }}>
                    <StarIcon /> Become a creator
                    </Container>}
                <Container className={`fluid btn btn-primary ${loading && `disabled`}`} onClick={!loading && updateSettings} style={{ margin: "20px", width: "auto" }}>
                    <SaveIcon /> Save changes
                    </Container>

            </div>
        </>)
    }

}


function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
  }

function isImage(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
      case 'jpg':
      case 'gif':
      case 'bmp':
      case 'png':
        //etc
        return true;
    }
    return false;
  }

function addProfilePic(file, setPictureUpload) {
    setPictureUpload(file)
    var reader = new FileReader()
    var html = document.getElementById('avatar')
    reader.onload = function (e) {
        
        if(isImage(file.name)){
            html.innerHTML = `
            
            <img height="100px" width="200px" style="object-fit: cover;" src='${e.target.result}' />
            
            `
          }
    }
    reader.readAsDataURL(file)
}

function addBanner(file, setBannerUpload) {
    setBannerUpload(file)
    var reader = new FileReader()
    var html = document.getElementById('banner')
    reader.onload = function (e) {
        
        if(isImage(file.name)){
            html.src = e.target.result;
          }
    }
    reader.readAsDataURL(file)
}