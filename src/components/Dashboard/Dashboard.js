import { CircularProgress, Tab, Tabs } from "@material-ui/core";
import React from "react";
import {Switch, Route} from "react-router-dom"
import CreatorDashboard from "./CreatorDashboard";
import Settings from "./Settings";
import UserDashboard from "./Userdashboard";
import SettingsIcon from '@material-ui/icons/Settings';
import DashboardIcon from '@material-ui/icons/Dashboard';

import firebase from "firebase/app";
import "firebase/auth";
import "firebase/functions";

import logo from "../../Logo.png"

export default function Dashboard({user}){
    const [value, setValue] = React.useState(0);
    const [loading, setLoading] = React.useState(true);
    const [settings, setSettings] = React.useState(null)

    const handleChange = (event, newValue) => {
      setValue(newValue);
    };

    const fetch = async () => {
        setLoading(true)
        const getSettings = firebase.functions().httpsCallable("getUserSettings");
        const settings = await getSettings(user.username)
        console.log(settings)      
        setSettings(settings.data.user ? settings.data.user : "error")
        setLoading(false)
}

    React.useEffect(() => {
        fetch()
    },[])

    if(!loading){
        return(<>
            <h3 style={{textAlign:"left", color:"white", marginLeft:"30px"}}>Dashboard</h3>
            <Tabs
            value={value}
            onChange={handleChange}
            variant="fullWidth"
            indicatorColor="primary"
            textColor="primary"
            aria-label="icon tabs example"
          >
            <Tab icon={<DashboardIcon/>} aria-label="UserDashboard" />
            <Tab icon={<SettingsIcon/>} aria-label="Settings" />
            {user.verifiedStatus>=2 && <Tab icon={<img width="30px" src={logo}/>} aria-label="CreatorDashboard" />}
          </Tabs>
            {value === 0 && <UserDashboard user={user}/>}
            {value === 1 && <Settings user={user} settings={settings}/>}
            {value === 2 && <CreatorDashboard user={user}/>}
            
            
            
                
            </>)
    } else {
        return(<><CircularProgress/></>)
    }
    
}