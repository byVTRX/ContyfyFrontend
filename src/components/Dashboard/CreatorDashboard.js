import React, { useEffect, useState } from "react";

import firebase from "firebase/app";
import "firebase/auth";
import "firebase/functions";

import {useHistory} from "react-router-dom"

import DateRangeIcon from '@material-ui/icons/DateRange';


import "./assets/scss/argon-dashboard-react.scss";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "./assets/plugins/nucleo/css/nucleo.css";
import "./assets/css/argon-dashboard-react.css"

import Skeleton from '@material-ui/lab/Skeleton';

import classnames from "classnames";
// javascipt plugin for creating charts
import Chart from "chart.js";
// react plugin used to create charts
import { Line, Bar } from "react-chartjs-2";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
  CardTitle,
} from "reactstrap";

// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2,
} from "./variables/charts.js";
import { CircularProgress } from "@material-ui/core";
/*const useStyles = makeStyles(theme => ({

    root: {
        [theme.breakpoints.up("md")]: {
            display: "grid",
            gridTemplateRows: "20% 80%",
            gridTemplateColumns: "25% 75%",
            width: "100%",
            padding: "30px",
        },
        [theme.breakpoints.down("md")]: {
            display: "flex",
            flexDirection: "column",
            width: "100%"
        },
        height: "90vh",
        
    },
    revenuewidget: {
        boxShadow: "0 6px 6px rgba(0,0,0,0.4)",
        gridColumnStart: "1",
        gridColumnEnd: "2",
        height: "100%",
        gridRowStart: "1",
        gridRowEnd: "2",
        padding: "20px",
        borderRadius: "20px",
        margin:"10px",
    },

    subscriptionswidget: {
        boxShadow: "0 6px 6px rgba(0,0,0,0.4)",
        gridColumnStart: "2",
        gridColumnEnd: "3",
        height: "100%",
        gridRowStart: "1",
        gridRowEnd: "2",
        padding: "20px",
        borderRadius: "20px",
        margin:"10px",
    },
    widgetTitle: {
        textAlign: "left",
        display: "flex",
        fontSize: "20px",
        color: "white",
        alignItems:"baseline"
    },
    widgetContent: {
        display: "flex",

        marginTop: "30px",
        justifyContent: "center",
        height: "100%"
    },
    coynText: {
        color: "white"
    },
    creatorMessage:{
        cursor:"pointer",
        background:"linear-gradient(90deg, rgba(255,146,1,1) 0%, rgba(251,34,34,1) 81%)",
        borderRadius:"20px",
        height:"40px",
        width:"100%",
        justifyContent:"center",
        alignItems:"center",
        border:"1px solid white"
    }


}))*/

export default function CreatorDashboard({}){
    //const classes = useStyles();
    const history = useHistory();
    const [activeNav, setActiveNav] = useState(1);
    const [chartExample1Data, setChartExample1Data] = useState("data2");
    const [dashboardData, setDashboardData] = useState(null) 

  if (window.Chart) {
    parseOptions(Chart, chartOptions());
  }

  const toggleNavs = (e, index) => {
    e.preventDefault();
    setActiveNav(index);
    setChartExample1Data("data" + index);
  };

  const fetchData = async () => {
    try {
      const getDashboardData = firebase.functions().httpsCallable('getCreatorDashboardData');
    const res =  await getDashboardData();
    const data = res.data;
    if(data.status !== 200 || !data.status){
      setDashboardData("error")
    } else {
      setDashboardData(data.creatorDashboard);
    }
    } catch (error) {
      setDashboardData("error")
    }
    
  }

  useEffect(() => {
    fetchData();
  }, [])


  if(dashboardData === "error"){
    return(<>
    <Container fluid style={{justifyContent:"center", display:"flex", height:"100vh"}}>
      <h3>Creator data failed to load. Please Try again later.</h3>
    </Container>
    </>)
    
  } else if (dashboardData === null || dashboardData === undefined){
    return(<>
    <Container className="mt-1" fluid>
      <Skeleton animation="wave"  fluid/>
      <Skeleton animation="wave"  fluid/>
      <Skeleton animation="wave"  fluid/>
    </Container>
    </>)
    
  } else {
    return(<>
    




      <Container className="mt-1" fluid>
          <Row className="mb-5" style={{borderBottom:"1px solid white"}}>
            <Col className="mb-5 mb-xl-5" xl="8">
              <Card className="bg-dark shadow">
                <CardHeader className="bg-transparent">
                  <Row className="align-items-center">
                    <div className="col">
                      <h6 className="text-uppercase text-light ls-1 mb-1">
                        Overview
                      </h6>
                      <h2 className="text-white mb-0">Total Revenue</h2>
                    </div>
                    <div className="col">
                      <Nav className="justify-content-end" pills>
                        <NavItem>
                          <NavLink
                            className={classnames("bg-warning py-2 px-3", {
                              active: activeNav === 1,
                              "bg-primary": activeNav === 1,
                            })}
                            href="#pablo"
                            onClick={(e) => toggleNavs(e, 1)}
                          >
                            <span className="text-dark d-none d-md-block">Month</span>
                            <span className="text-dark d-md-none">M</span>
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames("bg-warning py-2 px-3", {
                              active: activeNav === 2,
                              "bg-primary": activeNav === 2
                            })}
                            data-toggle="tab"
                            href="#pablo"
                            onClick={(e) => toggleNavs(e, 2)}
                          >
                            <span className="text-dark d-none d-md-block">Year</span>
                            <span className="text-dark d-md-none">W</span>
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames("bg-warning py-2 px-3", {
                              active: activeNav === 2,
                              "bg-primary": activeNav === 2
                            })}
                            data-toggle="tab"
                            href="#pablo"
                            onClick={(e) => toggleNavs(e, 2)}
                          >
                            <span className="text-dark d-none d-md-block"><DateRangeIcon/></span>
                            
                          </NavLink>
                        </NavItem>
                      </Nav>
                    </div>
                  </Row>
                </CardHeader>
                <CardBody>
                  {/* Chart */}
                  <div className="chart">
                    <Line
                      data={chartExample1[chartExample1Data]}
                      options={chartExample1.options}
                      getDatasetAtEvent={(e) => console.log(e)}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
            
            <Col xl="4" className="mb-5">
            <Card className="card-stats bg-dark shadow mb-5 mb-xl-5">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Monthly Revenue
                          </CardTitle>
                          <span className="h2 font-weight-bold mb-0">
                            €0
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-danger text-white rounded-circle shadow">
                            <i className="fas fa-dollar-sign" />
                          </div>
                        </Col>
                      </Row>
                      <p className="mt-3 mb-0 text-muted text-sm">
                        <span className="text-success mr-2">
                          <i className="fa fa-arrow-up" /> 0%
                        </span>{" "}
                        <span className="text-nowrap text-muted">Since last month</span>
                      </p>
                    </CardBody>
                  </Card>
                  <Card className="card-stats bg-dark shadow mb-5 mb-xl-5">
                    <CardBody>
                      <Row >
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Monthly Subscribers
                          </CardTitle>
                          <span className="h2 font-weight-bold mb-0">
                            +0
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-danger text-white rounded-circle shadow">
                            <i className="fas fa-users" />
                          </div>
                        </Col>
                      </Row>
                      <p className="mt-3 mb-0 text-muted text-sm">
                        <span className="text-success mr-2">
                          <i className="fas fa-arrow-up" /> 0%
                        </span>{" "}
                        <span className="text-nowrap text-muted">Since last month</span>
                      </p>
                    </CardBody>
                  </Card>
  
                  <Card className="card-stats bg-dark shadow mb-5 mb-xl-5">
                    <CardBody>
                      <Row>
                        <div className="col">
                          <CardTitle
                            tag="h5"
                            className="text-uppercase text-muted mb-0"
                          >
                            Creator Rank
                          </CardTitle>
                          <span className="h2 font-weight-bold mb-0">
                            999
                          </span>
                        </div>
                        <Col className="col-auto">
                          <div className="icon icon-shape bg-danger text-white rounded-circle shadow">
                            <i className="ni ni-favourite-28" />
                            
                          </div>
                        </Col>
                      </Row>
                      <p className="mt-3 mb-0 text-muted text-sm">
                        <span className="text-danger mr-2">
                          <i className="fas fa-arrow-down" /> 0
                        </span>{" "}
                        <span className="text-nowrap text-muted">Since last month</span>
                      </p>
                    </CardBody>
                  </Card>
  
              
            </Col>
          </Row>
         <Row style={{borderBottom:"1px solid white"}}>
  
  
  
         <Col className="mb-5 mb-xl-5" xl="8">
              <Card className="bg-dark shadow">
                <CardHeader className="bg-transparent">
                  <Row className="align-items-center">
                    <div className="col">
                      <h6 className="text-uppercase text-light ls-1 mb-1">
                        Overview
                      </h6>
                      <h2 className="text-white mb-0">Total Subscribers</h2>
                    </div>
                    <div className="col">
                      <Nav className="justify-content-end" pills>
                        <NavItem>
                          <NavLink
                            className={classnames("py-2 px-3", {
                              active: activeNav === 1,
                            })}
                            href="#pablo"
                            onClick={(e) => toggleNavs(e, 1)}
                          >
                            <span className="d-none d-md-block">Month</span>
                            <span className="d-md-none">M</span>
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames("py-2 px-3", {
                              active: activeNav === 2,
                            })}
                            data-toggle="tab"
                            href="#pablo"
                            onClick={(e) => toggleNavs(e, 2)}
                          >
                            <span className="d-none d-md-block">Year</span>
                            <span className="d-md-none">W</span>
                          </NavLink>
                        </NavItem>
                      </Nav>
                    </div>
                  </Row>
                </CardHeader>
                <CardBody>
                  {/* Chart */}
                  <div className="chart">
                    <Line
                      data={chartExample1[chartExample1Data]}
                      options={chartExample1.options}
                      getDatasetAtEvent={(e) => console.log(e)}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
  
          <Col xl="4">
          <Card className="bg-dark shadow">
                <CardHeader className="bg-transparent">
                  <Row className="align-items-center">
                    <div className="col">
                      <h6 className="text-uppercase text-muted ls-1 mb-1">
                        Performance
                      </h6>
                      <h2 className="mb-0">Total Subscribers</h2>
                    </div>
                  </Row>
                </CardHeader>
                <CardBody>
                  {/* Chart */}
                  <div className="chart">
                    <Bar
                      data={chartExample2.data}
                      options={chartExample2.options}
                    />
                  </div>
                </CardBody>
              </Card>
          </Col>
            
  
  
         </Row>
            <Col className="mb-5 mb-xl-0 mt-5" xl="12">
              <Card className="bg-dark shadow">
                <CardHeader className="bg-dark border-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">Last Supporters</h3>
                    </div>
                    <div className="col text-right">
                      <Button
                        color="primary"
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                        size="sm"
                      >
                        See all
                      </Button>
                    </div>
                  </Row>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="bg-dark">
                    <tr>
                      <th scope="col">Supporter</th>
                      <th scope="col">amount paid</th>
                      <th scope="col">subscribed (since)</th>
                      <th scope="col"></th>
                    </tr>
                  </thead>
                  <tbody>
                   {/**
                    <tr>
                      <th scope="row">John doe</th>
                      <td>$4,890</td>
                      <td>01/02/2021</td>
                      <td>
                        <Button className="btn btn-primary">Profile</Button>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">Mary doe</th>
                      <td>$4,200</td>
                      <td>Not subscribed</td>
                      <td>
                        <Button className="btn btn-primary">Profile</Button>
                      </td>
                    </tr>
                    */} 
                    
                  </tbody>
                </Table>
              </Card>
            </Col>
            <Col xl="12" className="mt-5">
              <Card className="shadow bg-dark">
                <CardHeader className="border-0 bg-dark">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">Payouts</h3>
                    </div>
                    <div className="col text-right">
                      <Button
                        color="primary"
                        href="#pablo"
                        onClick={(e) => e.preventDefault()}
                        size="sm"
                      >
                        See all
                      </Button>
                    </div>
                  </Row>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="bg-dark">
                    <tr>
                      <th scope="col">Payout ID</th>
                      <th scope="col">Requested</th>
                      <th scope="col">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    {dashboardData.payouts.map((payout) => {
                      const payoutDate = new Date(payout.created);
                      return(
                      <tr>
                        <th scope="row">{payout.id}</th>
                        <td>{payoutDate.getMonth() + "/" + payoutDate.getDay() + "/" + payoutDate.getFullYear()}</td>
                        <td>

                        <Button className="btn btn-outline-primary">{payout.status}</Button>

                        </td>
                      </tr>
                      )
                      
                    })}
                   
                  </tbody>
                </Table>
              </Card>
            </Col>
         
        </Container>
  
  
  
      
  </>)
  
  }
    }

