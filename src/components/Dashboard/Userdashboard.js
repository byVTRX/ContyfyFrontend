import React from "react";

import firebase from "firebase/app";
import "firebase/auth";
import "firebase/functions";
import { List, makeStyles, Typography } from "@material-ui/core";

import {useHistory} from "react-router-dom";
import TransactionList from "../Payment/TransactionlistComponent";





const useStyles = makeStyles(theme => ({

    root: {
        [theme.breakpoints.up("md")]: {
            display: "grid",
            gridTemplateRows: "20% 60% 20%",
            gridTemplateColumns: "25% 73%",
            width: "100%",
            padding: "30px",
        },
        [theme.breakpoints.down("md")]: {
            display: "flex",
            flexDirection: "column",
            width: "100%",
            padding:"20px"
        },
        height: "90vh",
        gridGap:"20px",
        width:"100%"
    },
    coynwidget: {
        boxShadow: "0 6px 6px rgba(0,0,0,0.4)",
        gridColumnStart: "1",
        gridColumnEnd: "2",
        height: "100%",
        gridRowStart: "1",
        gridRowEnd: "2",
        padding: "20px",
        borderRadius: "20px",
        
        backgroundColor:"transparent"
    },

    subscriptionswidget: {
        boxShadow: "0 6px 6px rgba(0,0,0,0.4)",
        gridColumnStart: "2",
        gridColumnEnd: "3",
        height: "100%",
        gridRowStart: "1",
        gridRowEnd: "2",
        padding: "20px",
        borderRadius: "20px",
        
        backgroundColor:"transparent"
    },
    widgetTitle: {
        textAlign: "left",
        display: "flex",
        fontSize: "20px",
        color: "white",
        alignItems:"baseline"
    },
    widgetContent: {
        display: "flex",

        marginTop: "30px",
        justifyContent: "center",
        height: "100%"
    },
    coynText: {
        color: "white"
    },
    creatorMessage:{
        cursor:"pointer",
        background:"linear-gradient(90deg, rgba(255,146,1,1) 0%, rgba(251,34,34,1) 81%)",
        borderRadius:"20px",
        height:"40px",
        width:"100%",
        justifyContent:"center",
        alignItems:"center",
        border:"1px solid white",
        textAlign:"center",
    },
    transactionswidget:{
        gridColumnStart:"1",
        gridColumnEnd:"3",
        gridRowStart:"2",
        gridRowEnd:"3",
        boxShadow:"0 6px 6px rgba(0,0,0,0.4)",
        padding:"5px",
       
        borderRadius: "20px",
        alignItems:"center",
        justifyContent:"center",
        minHeight:"200px",
        backgroundColor:"transparent"
    }


}))

export default function UserDashboard({ user }) {
    const classes = useStyles();
    const history = useHistory();



    return (<>

     

    
    
    
    <div className={classes.root}>
       

        <div className={classes.coynwidget}>
            <h6 className={classes.widgetTitle}>Coyns  <small className="   " style={{fontSize:"9px", marginLeft:"2px", background:"transparent",borderRadius:"5px", border:"1px solid orange", color:"white", padding:"2px"}}> Coming soon</small></h6>
            
            <div className={classes.widgetContent}>
                <Typography variant="h4" className={classes.coynText}>0</Typography>
                
            </div>
        </div>

        <div className={classes.subscriptionswidget}>
            <h6 className={classes.widgetTitle}>Subscriptions</h6>
            <div className={classes.widgetContent}>
                <List className={classes.subscriptionslist}>
                    <h3 style={{color:"white", fontSize:"24px"}}>No subscriptions found</h3>
                </List>
            </div>
        </div>

        <div className={classes.transactionswidget}>
            <TransactionList transactions={[]}/>
        </div>

    </div>
    </>)
}

