import React, {Component} from "react"
import {Router, Route, Switch, useHistory} from "react-router-dom"
import Confirm from "./ConfirmComponent"
import SignUp from "./SignUpComponent"
import Login from "./LoginComponent"
import NotFound from "./NotFoundComponent"

export default function Authentication(){
    
        

        return(
            <div className="outer">
                <div className="inner">
                
                <Switch>
                <Route path="/confirm" component={Confirm}/>
                <Route path="/sign-up" component={SignUp} />
                <Route path="/sign-in" component={Login} />
                <Route exact path="/" component={Login} />
                <Route path="/*" component={NotFound}/> 
                </Switch>
                
                
                </div>
            </div>
                
                
            
            
            
        )
    
}