import React, { useEffect, useState } from 'react';

import firebase from 'firebase';
import "firebase/auth"
import "firebase/functions"

import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Skeleton from '@material-ui/lab/Skeleton';




import { makeStyles } from '@material-ui/core/styles';
import {
  Button,
  List,
  ListItem,
  Divider,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Typography,
  CircularProgress,
} from '@material-ui/core';

import { useHistory } from 'react-router';
import moment from 'moment';

import reactStringReplace from 'react-string-replace';

const useStyles = makeStyles(theme => ({
  listRoot: {
    width: '100%',
    wordBreak: 'break-all',
    overflow: 'scroll',
    borderRight: '1px solid #37444C',
  },
  alignCenter: {
    textAlign: 'center',
  },
  loader: {
    textAlign: 'center',
    paddingTop: 20,
  },
  maxWidth: {
    width: '100%',
  },
  listHeader: {
    position: 'sticky',
    top: 0,
    zIndex: 1200,
    backgroundColor: '#15202B',
    borderBottom: '1px solid #37444C',
  },
  clickable: {
    cursor: 'pointer',
  }
}));

export default function NotificationList({isLoading,notifications, user }) {
  const classes = useStyles();



  
  return (
    <div className={classes.listRoot}>
      {isLoading ?
        <div className={classes.loader}>
          <CircularProgress size={25} />
        </div>
        :
        <List disablePadding style={{heigth:"100%"}} >
          
            
          
          {notifications.map(notification => (
            <span key={notification.id}>
              <NotificationItem notification={notification} />
              <Divider component="li" />
            </span>
          ))}
          
        </List>
      }
    </div>
  )
}




function NotificationItem({ notification }) {
  const classes = useStyles();
  const history = useHistory();
  const now = moment();
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState(null)
  
  useEffect(() => {

    const fetch = async () => {
      setLoading(true);
      
      var getUser = await firebase.functions().httpsCallable('getUser');
      const userData = await getUser({username:notification.issuer});
      console.log(userData);  
      if(userData.data.status === 200){
        setUser(userData.data.user);
      } 
  
      setLoading(false)
    }

   fetch()
  },[])
   
  
  
  const calcTimestampDiff = (timestamp) => {
    const time = new Date(timestamp).getTime();
    const scales = ['years', 'months', 'weeks', 'days', 'hours', 'minutes', 'seconds'];

    for (let i=0; i < scales.length; i++){
      const scale = scales[i];
      const diff = moment(now).diff(time, scale);
      if( diff > 0) return diff + scale.charAt(0)
    }

    return 0 + scales[scales.length - 1].charAt(0)
  }

  
    return (<React.Fragment>
      <ListItem alignItems='flex-start' style={{height:"auto", margin:0, paddingBottom:0, paddingTop:0}}>
            <ListItemAvatar>
              <div className={classes.clickable}>
                <Avatar style={{width:"50px", height:"50px", marginRight:"20px"}} height=""alt="!" src={user ? user.profilepicture : null} onClick={() => {history.push('/profile/' + notification.issuer)}}/>
              </div>
            </ListItemAvatar>
            <ListItemText
              primary={
                <React.Fragment>
                  
                  <p
                    
                    style={{display:'inline', color:"white"}}
                  >
                    {!user ? <Skeleton width="30" height="20"/>  : user.displayName }
                    {!user ? <Skeleton width="20" height="10"/> : <small style={{fontSize:"small", color:"grey"}}>{" @" + user.username}</small>}
                  </p>
                </React.Fragment>
              }
              secondary={
         
                <React.Fragment style={{display:'inline-flex', justifyContent:"space-between", marginBottom:"10px", color:"white", width:"100%", height:"20px"}}>
                  <p style={{color:"white", margin:0}} id="notificationText">
                  {notification.type === "comment" && 
                  reactStringReplace(notification.content, "commented", (match, i) =>(
                    <span style={{color: "orange"}}>{match}</span>
                  ))
              }
      
              {   notification.type === "like" && 
                  reactStringReplace(notification.content, "liked", (match, i) =>(
                    <span style={{color: "orange"}}>{match}</span>
                  ))
                }
      
      
                {   notification.type === "subscription" && 
                  reactStringReplace(notification.content, "subscribed", (match, i) =>(
                    <span style={{color: "orange"}}>{match}</span>
                  ))
                }

                {   notification.type === "tip" && 
                  reactStringReplace(notification.content, "tipped", (match, i) =>(
                    <span style={{color: "orange"}}>{match}</span>
                  ))
                }
      
                  
                  
                </p>
                {notification.type === "comment" && <small style={{color:"grey"}}>"{notification.commentText}"</small>}
                </React.Fragment>
                
                  
                
               
                
              }
            />
            <ListItemSecondaryAction style={{paddingRight:"20px"}}>
            {calcTimestampDiff(notification.timestamp*1000)}
            </ListItemSecondaryAction>
          </ListItem>
        </React.Fragment>
    )    
  
 
  
}

