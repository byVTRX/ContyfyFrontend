import React, { Component } from "react";



import {useHistory} from "react-router-dom"

import $ from "jquery"
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root:{
        background:"white",
        maxWidth:"400px",
        width:"20%"
    }
}))

export default function Login() {
    const history = useHistory()
    const classes = useStyles();    
    return (<div>
            <form onSubmit={async (event) => {
                event.preventDefault()
                var confirmSuccessfull = false;
                confirmSuccessfull = await confirmAcc($('#username').val(), $('#code').val())
                if(confirmSuccessfull){
                    history.push('/') 
                }
            }} >
                <div className={classes.root}>
                <h3>Log in2</h3>

<div className="form-group">
    <label>Email</label>
    <input type="text" className="form-control" placeholder="Enter email" id="username"/>
</div>

<div className="form-group">
    <label>Code</label>
    <input type="text" className="form-control" placeholder="Enter confirmation code" id="code"/>
</div>



<button type="submit" className="btn btn-dark btn-lg btn-block">Confirm</button>
<p className="forgot-password text-right">
    Did not receive code? <button className="btn btn-dark btn-sm btn-block" onClick={(event) => {
        event.preventDefault();
        //RESEND CONFIRMATION
    }}>Resend it</button>
</p>
                </div>
                
            </form>
        </div>);
    
}

async function confirmAcc(email, code) {
    try {
        const res = null;//CONFIRM ACCOUNT
        console.log(res)
        return true
    } catch (error) {
        alert(error)
        console.error(error)
        return false;
    }
}