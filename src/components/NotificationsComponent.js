import React, {useState, useEffect, useReducer} from "react";

import {useHistory} from "react-router-dom"
import "./main.css"
import "bootstrap/dist/css/bootstrap.min.css"
import firebase from "firebase";
import "firebase/functions"
import {
  Button,
  List,
  ListItem,
  Divider,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Typography,
  CircularProgress,
} from '@material-ui/core';

import _ from "lodash"

import NotificationList from "./NotificationList"





export default function Notifications({user}) {
    
  
  const [nextToken, setNextToken] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [notifications, setNotifications] = useState([])

  const currentUser = user;
  

  useEffect(() => {
    const fetchNotifications = async () => {
      setIsLoading(true)
      var getNotifications = firebase.functions().httpsCallable('getNotifications')
      var notificationsObj = await getNotifications()
      
     

      if(notificationsObj.data.status === 200){
        setNotifications(notificationsObj.data.notifications)
        console.log(notifications)
      } else {
        setNotifications([])
      }
      setIsLoading(false)

    }

    fetchNotifications();
  },[])

    

    
      
      
      
      return(<>
      <h3 style={{textAlign: 'left', color:"white", marginLeft:"30px"}}>Notifications</h3>
      <Divider/>
      <div style={{margin:"10px 0"}}>
              
              
              
              
              
              {notifications.length > 0 ? <NotificationList
                  isLoading={isLoading}
                  notifications={notifications}
                  
                  listHeaderTitle={'Feed'}
                  user={currentUser}
                  /> : <div className="text-center">No Notifications found</div>}
              
              
              
          </div>
      </>);
    
}









