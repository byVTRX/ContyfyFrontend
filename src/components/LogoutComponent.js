import { CircularProgress } from "@material-ui/core"
import React from "react"
import {useHistory, Link} from "react-router-dom"
import firebase from "firebase/app";
import "firebase/auth";




export default function Logout() {
    const history = useHistory();

    LogOut(history)
    return(
        <div className="text-center">
            <h2>You are being logged out</h2>
            <CircularProgress className="" />
        </div>
    )
}


async function LogOut(history) {
    const res = await firebase.auth().signOut();
    return history.push('/sign-in')
}