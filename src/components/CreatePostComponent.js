import React, { useEffect, useState } from "react"

import $ from "jquery"
import firebase from "firebase"
import "firebase/auth";
import "firebase/storage";
import "firebase/firestore";
import crypto from "crypto";
import alertify from "alertifyjs";
import "./main.css"


import {
  Button,
  List,
  ListItem,
  Divider,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Typography,
  CircularProgress,
  CardContent,
  CardHeader,
  CardActions,
  IconButton,
  CardMedia,
  Collapse,
  makeStyles,
  TextField,
  Card,
  Grid,
  Tooltip,
} from '@material-ui/core';

import ClearIcon from '@material-ui/icons/Clear';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import SendIcon from '@material-ui/icons/Send';
import Switch from '@material-ui/core/Switch';
import ModeCommentOutlinedIcon from '@material-ui/icons/ModeCommentOutlined';
import AttachMoneyOutlinedIcon from '@material-ui/icons/AttachMoneyOutlined';
import MoneyOffOutlinedIcon from '@material-ui/icons/MoneyOffOutlined';


const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: "40%",
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
  root: {
    maxWidth:"600px",
    minWidth:345,
    borderRadius:0,
    
    
  },
  action: {
    display:"flex",
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-evenly",
  },
  form: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
  postActions: {
    display:"flex",
    width:"100%",
    justifyContent:"space-between",
    padding:"24px 24px",
  },
  file: {
    display:"none"
  },
  get_file:{
    border: "5px solid #88c",
    padding: "15px",
    borderRadius: "5px",
    margin: "10px",
    cursor: "pointer",
  },
  textarea: {
    border: "none",
    width: "100%",
    fontSize: "16px",
    marginTop: "8px",
    background:"transparent",
    color:"white",
    margin:"0 auto",
    resize:"none"
  },
  textareaFocused: {
    border:"none",
  },
  
}));

const CreatePost = ({user}) => {
    
    
    const classes = useStyles();
    const [fileUpload, setFileUpload] = useState(null);
    const [value, setValue] = useState('');
    const [isLoading, setLoading] = useState(false);
    const [uploadProgress, setUploadProgress] =  useState(0)
    const [adultContent, setAdultContent] = useState(false);
    const [allowComments, setAllowComments] = useState(false);
    const [allowTips, setAllowTips] = useState(false);
    

    {/** 
    const onPost = async (text) => {
        try {
            
            Auth.currentUserInfo().then(async info => {
                var dispName = info.attributes.username

                const res = await API.graphql(graphqlOperation(createPost, { input: {
                    type: 'post',
                    postType:'text',
                    title: text,
                    displayName: dispName,
                  }})); 
            })
             
        } catch (error) {
            console.error(error)
        }
        
        
        
      }
*/}
      
const sendPost = async (file, data) => {
  if(file){
  
    var mediaType = "";
    var valid = false;
    var storageDir = "images/";
    if(isVideo(file.name)){
      //TODO: CHANGE BUCKET NAME DEPENDING ON ENV
      storageDir = "videos/"
      mediaType="video";
      valid = true;
    } else if(isImage(file.name)){
      mediaType="image"
      valid = true
    } else {
      alert("FileType is not supported! Please use '.mp4', '.m4v', '.avi', '.mpg' or '.jpg', '.gif', '.bmp', '.png'")
      valid = false
    }
    if(valid){
      var id = crypto.randomBytes(30).toString('hex');
      
      
        


        const put = () => {
          const upload = firebase.storage().ref(storageDir + `${id}.` + getExtension(file.name)).put(file)
          upload.on("state_changed", (snapshot) => {
            setUploadProgress(Math.round(snapshot.bytesTransferred/snapshot.totalBytes * 100))
          }, (error) => {alertify.error('Your upload failed. Please try again')}, 
          () => {

            firebase.storage().ref(storageDir.replace('/','')).child(`${id}.${getExtension(file.name)}`).getDownloadURL().then((fileUrl) => {
              
              const post = {
                username:firebase.auth().currentUser.uid,
                title:value,
                timestamp:Math.floor(Date.now()/1000),
                likes:[],
                comments:[],
                media:fileUrl,
                adultContent:adultContent,
                allowTips:allowTips,
                allowComments:allowComments,
                postType:mediaType,
              }
              
              var createPost = firebase.functions().httpsCallable('createPost');
              createPost({post:post})
              



              



            })
            
          })
        }
        
        put();

        
      
      
    }
    
  } else {
    const post = {
      username:firebase.auth().currentUser.uid,
      title:value,
      timestamp:Math.floor(Date.now()/1000),
      likes:[],
      comments:[],
      media:null,
      adultContent:adultContent,
      allowTips:allowTips,
      allowComments:allowComments,
      postType:"text",
    }
    var createPost = firebase.functions().httpsCallable('createPost')
    createPost({post:post})
  }

  
  
}

  
    return(
   

      <Card elevation={0} style={{background:"transparent", borderBottom:"1px solid white"}}>
      <CardHeader>
        <Typography variant="caption">Create Post</Typography>
      </CardHeader>
      <CardContent  style={{width:"100%"}}>

        <form className={classes.form}>
        <p>Create Post</p>
        <textarea 
        className={classes.textarea}
        name="create a post" 
        rows={2} 
        value={value}
        placeholder="Enter a caption..." 
        onChange={(e) => setValue(e.target.value)} 
        draggable={false}
        
        maxLength={150}
        />

        </form>
        <div id="content"></div>

        
      </CardContent>
      <CardActions >
        <div className={classes.postActions} style={{display:"inline-flex"}}>

        <div>
          <IconButton id="get_file" value="Grab File" onClick={(e) => {
          e.preventDefault()
          document.getElementById('file').click();
          }}>
          <AddAPhotoIcon style={{color:"white"}}/>

          </IconButton>
          <input  type="file" id="file" placeholder="" className={classes.file} onChange={(e) => {e.preventDefault();
          if(fileUpload){
            setFileUpload(null)
            
            document.getElementById('media_preview').remove()
            setFileUpload(e.target.files[0])
            addFileToPost(e.target.files[0])
            
          } else {
            setFileUpload(e.target.files[0])
            addFileToPost(e.target.files[0])
          }
         
        

         
         }}/>

         {fileUpload && 
         <Tooltip title="Remove media from post" style={{fontSize:"30px"}}>
          <IconButton style={{width:"50px"}} color="secondary" onClick={(e) => {
            e.preventDefault();
            setFileUpload(null)
            document.getElementById('file').value = "";
            document.getElementById('media_preview').remove()
          }}>

            {/**ICON GROESSER */}
            <ClearIcon color="secondary" className="2x"/>
          </IconButton>
         </Tooltip>
         
         }
<Tooltip title={allowComments ? "Comments are enabled" : "Comments are disabled"}>
         {allowComments ? 
         <IconButton color="primary" onClick={(e) => setAllowComments(false)}><ModeCommentOutlinedIcon style={{color:"green"}} /></IconButton> : 
         <IconButton color="secondary" onClick={(e) => setAllowComments(true)}><ModeCommentOutlinedIcon style={{color:"red"}} /></IconButton>}
         </Tooltip>
         
         <Tooltip title={allowTips ? "Tips are enabled" : "Tips are disabled"}>
         {allowTips ? 
         <IconButton color="primary" onClick={(e) => setAllowTips(false)}><AttachMoneyOutlinedIcon  style={{color:"green"}} /></IconButton> : 
         <IconButton color="secondary" onClick={(e) => setAllowTips(true)}><MoneyOffOutlinedIcon  style={{color:"red"}} /></IconButton>}
         </Tooltip>
          
          {/**<Switch
            checked={adultContent}
            onChange={() => setAdultContent(!adultContent)}
            name="adultContent"
            inputProps={{ 'aria-label': 'secondary checkbox' }}
          />
        
                  <Typography variant="caption">This post containes {!adultContent && <b style={{color:"darkred", fontSize:"14px "}}>no</b>} adult content meant for viewers above the age of 18</Typography>
          **/}
         
         
         

         
         
        </div>
        

        

        <IconButton aria-label="send button" onClick={(e) => {
          e.preventDefault(); 

          if(!adultContent){

            alertify.defaults.transition = "slide";
            alertify.defaults.theme.ok = "btn btn-primary";
            alertify.defaults.theme.cancel = "btn btn-danger";
            alertify.defaults.theme.input = "form-control";

           alertify.confirm('Confirm post information', 
          "Please confirm that your post contains no form of adult content. This contains but is not limited to explicit nudity or graphic violence. You can read up on our \"Adult Content\" guideline on contyfy.com/tos. Note that any violation will lead to your account beeing suspended", 
          () => sendPost(fileUpload),
          () => alertify.error('Your post has not been sent. You can still make changes to it and send it.'))
          } else {
            sendPost(fileUpload)
          }
          
          }}>
          <SendIcon className={classes.send}/>

        </IconButton>
        </div>
      </CardActions>
      {uploadProgress > 0 && <div style={{display:"flex", justifyContent:"center", alignItems:"center", marginBottom:"50px"}}><CircularProgress variant="determinate" style={{color:uploadProgress === 100 ? "green" : "yellow", alignSelf:"center"}} value={uploadProgress} /> <span style={{alignSelf:"center"}}>{" " + uploadProgress + "%"}</span></div>}

    </Card>


  /**
  <div>
      <form  noValidate autoComplete="off">
      <TextField id="outlined-basic" label="Post title" variant="outlined" />
      
      </form>
     
        <IconButton aria-label="send button" onClick={(e) => {e.preventDefault(); sendPost();}}>
          <span className="svg-icon svg-icon-primary svg-icon-2x"><svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
          <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
          <rect x={0} y={0} width={24} height={24} />
          <path d="M3,13.5 L19,12 L3,10.5 L3,3.7732928 C3,3.70255344 3.01501031,3.63261921 3.04403925,3.56811047 C3.15735832,3.3162903 3.45336217,3.20401298 3.70518234,3.31733205 L21.9867539,11.5440392 C22.098181,11.5941815 22.1873901,11.6833905 22.2375323,11.7948177 C22.3508514,12.0466378 22.2385741,12.3426417 21.9867539,12.4559608 L3.70518234,20.6826679 C3.64067359,20.7116969 3.57073936,20.7267072 3.5,20.7267072 C3.22385763,20.7267072 3,20.5028496 3,20.2267072 L3,13.5 Z" fill="#000000" />
          </g>
          </svg></span>

        </IconButton>
        
      </div>
*/
      
  
    
    )
}

export default CreatePost;





const addFileToPost = (file) => {
  var reader = new FileReader();
  var html = document.createElement('div');
  html.id = "media_preview";
  reader.onload = function (e) {
    if(isImage(file.name)){
      html.innerHTML = `
      
      <img src='${e.target.result}' height='360px' width='640px'/>
      
      `
    }

    if(isVideo(file.name)){
      html.innerHTML = `
     
      <video controls height='360px' width='640px' src='${e.target.result}'</video>
      
      `
    }
    
    document.getElementById('content').appendChild(html)
  }
  reader.readAsDataURL(file)
}


function getExtension(filename) {
  var parts = filename.split('.');
  return parts[parts.length - 1];
}

function isImage(filename) {
  var ext = getExtension(filename);
  switch (ext.toLowerCase()) {
    case 'jpg':
    case 'gif':
    case 'bmp':
    case 'png':
      //etc
      return true;
  }
  return false;
}

function isVideo(filename) {
  var ext = getExtension(filename);
  switch (ext.toLowerCase()) {
    case 'm4v':
    case 'avi':
    case 'mpg':
    case 'mp4':
      // etc
      return true;
  }
  return false;
}