import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom"
import CreatePost from "./CreatePostComponent";
import logo from "../Logo.png"
import HomeIcon from '@material-ui/icons/Home';
import SendIcon from '@material-ui/icons/Send';
import NotificationsIcon from '@material-ui/icons/Notifications';
import DashboardIcon from '@material-ui/icons/Dashboard';

import "./main.css"

import { Avatar, BottomNavigation, BottomNavigationAction, Button, ClickAwayListener, IconButton, makeStyles, Menu, MenuItem, Typography } from "@material-ui/core"
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from "reactstrap";


const useStyles = makeStyles({
  root:{
    borderBottom:"1px solid white",
    height:"70px",
    display:"flex",
    flexDirection:"row",
    justifyContent:"space-between",
    alignItems:"center",
    margin:"0",
    padding:"20px",
    color:"white"

  },
  nav:{
    width:"100%",
    background:"transparent",
    
  },
  icon: {
    fontSize:"35px"
  }
})
const MobileHeader = ({user}) => {
  const history = useHistory();
  const [name, setName] = useState("")
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {

    const runAsync = async () => {

      
    }

    runAsync();
  }, [])


  return (<div className={classes.root} >

  <HomeIcon className={classes.icon} onClick={() => history.push('/')}/>
  <SendIcon className={classes.icon} onClick={() => history.push('/directmessages')}/>
  {user.verifiedStatus>=2 && "+"} 
  <NotificationsIcon className={classes.icon} onClick={() => history.push('/notifications')}/>
  <Avatar src={user.profilepicture} onClick={handleClick}/>
  <Menu
  id="simple-menu"
  anchorEl={anchorEl}
  keepMounted
  open={Boolean(anchorEl)}
  onClose={handleClose}
>
  <MenuItem onClick={()=>{handleClose();history.push('/profile')}}>Profile</MenuItem>
  <MenuItem onClick={()=>{handleClose();history.push('/dashboard')}}>Dashboard</MenuItem>
  <MenuItem onClick={()=>{handleClose();history.push('/logout')}}>Logout</MenuItem>
</Menu>
  

  </div>)


}





export default MobileHeader;

