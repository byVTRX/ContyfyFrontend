import React, { useEffect, useState } from 'react';

import { useHistory } from 'react-router';
import moment from 'moment';
import "./main.css"

import $ from "jquery"
import _ from "lodash"

import firebase from "firebase"

import verified from "../Verification.svg"
import { Avatar, Button, CircularProgress, Divider, IconButton, Link, makeStyles, Menu, MenuItem, TextField, Typography } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Skeleton from '@material-ui/lab/Skeleton';


const useStyles = makeStyles(theme => ({
    comment: {
        display: "inline-flex",
        justifyContent: "flex-start",
        alignItems: "center",
        width: "100%",
        textAlign: "start",
        minHeight: "10px"

    },
    commentList: {
        listStyle: "none",
        marginLeft: "15px",
        marginRight: "15px",
        padding: "0",
        marginBottom: "0px"
    },
    commentActions: {
        fontSize: "5px",
        right: 0,
        top: 0,
        display: "flex",

    }

}))

export default function CommentList({ user, isLoading, comments, post, postId }) {
    const classes = useStyles();





    return (<div>

        <Divider fullWidth style={{ marginBottom: "0px" }} />
        <ul className={classes.commentList} id="commentList">

            {isLoading ? <CircularProgress /> : comments.map(comment =>
                <span key={comment.id}>

                    <CommentItem user={user} post={post} postId={postId} commentId={comment.id} comment={comment.comment} />

                </span>

            )}
        </ul>

    </div>)
}



export function CommentItem({ comment, user, commentId, post, postId }) {
    const classes = useStyles();

    const now = moment();
    const [commentUser, setCommentUser] = useState({ displayName: "", id: comment.userId })
    const history = useHistory();
    const [menuAnchor, setMenuAnchor] = useState(null);
    const [loading, setLoading] = useState(false)
    const [allowDelete, setAllowDelete] = useState(false)
    
    const [liked, setLiked] = useState(false)
    const [likes, setLikes] = useState([]);

    const openMenu = (event) => {
        setMenuAnchor(event.currentTarget)
    }

    const handleCloseMenu = () => {
        setMenuAnchor(null);
    }

    const toggleCommentLike = async () => {
        if (!liked) {
            const likeComment = await firebase.functions().httpsCallable('likeComment');
            const res = await likeComment({ postId: postId, commentId, postUser: comment.creatorId, commentUser: comment.userId })
            if (res.data.status === 200) {
                setLiked(true)
                let newLikes = likes;
                newLikes.push({username:firebase.auth().currentUser.uid})
                setLikes(newLikes)
            }
        } else {
            const unLikeComment = await firebase.functions().httpsCallable('unLikeComment');
            const res = await unLikeComment({ postId: postId, commentId, postUser: comment.creatorId, commentUser: comment.userId })
            if (res.data.status === 200) {
                setLiked(false)
                let newLikes = likes.filter(like => like.username === firebase.auth().currentUser.uid);
                setLikes(newLikes)
            }
        }
    }

    const calcTimestampDiff = (timestamp) => {
        const time = new Date(timestamp * 1000).getTime();
        const scales = ['years', 'months', 'weeks', 'days', 'hours', 'minutes', 'seconds'];

        for (let i = 0; i < scales.length; i++) {
            const scale = scales[i];
            const diff = moment(now).diff(time, scale);
            if (diff > 0) return diff + scale.charAt(0)
        }

        return 0 + scales[scales.length - 1].charAt(0)
    }

    useEffect(() => {
        const fetch = async () => {
            setLoading(true)


            if ((user.username === comment.userId) || (user.username === comment.creatorId)) {
                setAllowDelete(true)
            }
            console.log(comment)
            var getUser = firebase.functions().httpsCallable('getUser');
            var userData = await getUser({ username: comment.userId })

            const data = userData.data.user

            var getCommentLikes = firebase.functions().httpsCallable('getCommentLikes');
            var commentLikes = await getCommentLikes({ commentId: commentId, creatorId: comment.creatorId, postId: comment.postId })

            

            if (commentLikes.data.status === 200) {
                
                var temp = commentLikes.data.likes;
                console.log(temp)
                setLikes(temp)
                if(temp.length >0 ){
                    temp.forEach(like => {
                        if(like.username === firebase.auth().currentUser.uid){
                            setLiked(true)
                        }
                    })
                }
            }

            


            setCommentUser(data);
            setLoading(false);
            
        }

        
        fetch()
        
    }, [])

    return (<React.Fragment>

        <li className={classes.comment} >


            {loading ? <>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Skeleton variant="circle" width={40} height={40} style={{ marginRight: "10px" }} />
                    <Skeleton variant="text" width={300} />
                </div>


            </> : <div style={{ display: "inline-flex", alignItems: "center", fontSize: "5px", width: "100%", justifyContent: "space-between", margin: "0 auto" }} >
                <div style={{ display: 'inline-flex', alignItems: 'center' }}>
                    <Link href={"/profile/" + commentUser.username}>
                        <Avatar src={commentUser.profilepicture} width={10} height={10} style={{ marginRight: "10px" }}></Avatar>
                    </Link>

                    <p style={{ color: "white", fontSize: "15px", marginTop: "15px" }}>

                        <Link href={"/profile/" + commentUser.username} style={{ color: "white", marginRight: "5px", fontWeight: "bold", fontSize: "15px" }}>{commentUser.displayName}</Link>
                        {comment.content}</p>
                </div>

                <div className={classes.commentActions}>
                    <Typography style={{ alignItems:"center"}}>
                        <IconButton style={{marginBottom: "10px"}} onClick={() => {
                            toggleCommentLike();
                        }}>
                            {liked ? <span className="fas fa-heart" style={{ color: "red", marginTop: "10px" }}></span> : <span style={{ marginTop: "10px"}}className="far fa-heart"></span>}
                        </IconButton>
                        
                        {likes.length > 0 && <span style={{fontSize: "15px", color: "white", marginRight:"5px"}}>{likes.length}</span>}
                        {String.fromCharCode(183) + ' ' + calcTimestampDiff(comment.timestamp) + ' ' + String.fromCharCode(183)}
                    </Typography>
                    <IconButton aria-label="settings" aria-controls="menu" aria-haspopup="true" onClick={openMenu}>
                        <MoreVertIcon />
                    </IconButton>
                    <Menu
                        id="menu"
                        anchorEl={menuAnchor}
                        keepMounted
                        open={Boolean(menuAnchor)}
                        onClose={handleCloseMenu}
                    >
                        <MenuItem onClick={handleCloseMenu}>Report</MenuItem>
                        {user.username === comment.userId && <MenuItem onClick={handleCloseMenu}>Edit Comment</MenuItem>}
                        {allowDelete && <MenuItem onClick={handleCloseMenu}>Delete Comment</MenuItem>}
                    </Menu>
                </div>

            </div>}




        </li>
        <Divider variant="fullWidth" />
    </React.Fragment>)
}