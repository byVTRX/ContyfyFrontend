import { Button, TextField } from "@material-ui/core";
import React, { useState } from "react";

import firebase from "firebase/app"
import "firebase/functions"




export default function TestComponent(){
    const [textVal, setTextVal] = useState("")
    const [priceVal, setPriceVal] = useState(499)

    const handleClick = async () => {
        const username = textVal;
        var approveCreator = firebase.functions().httpsCallable('approveCreator');

        approveCreator({uid:username}).then((res) => {
            alert(JSON.stringify(res))
        })
    } 

    const handlePriceChange = async () => {
        const username = textVal;
        const price = priceVal;
        var updatePrice = firebase.functions().httpsCallable('changePrice');

        updatePrice({uid:username,price:price*100}).then((res) => {
            alert(JSON.stringify(res))
        })
    }
    
    return(<>
    <div style={{height:"100px"}}></div>
        <TextField onChange={(e) => {
            setTextVal(e.target.value)
        }}/>
        <Button variant="outlined" onClick={handleClick} >Submit</Button>
        <br/>
        <TextField onChange={(e) => {
            setPriceVal(e.target.value)
        }} type="number"/>
        <Button variant="outlined" onClick={handlePriceChange} >Price</Button>
    </>)
}