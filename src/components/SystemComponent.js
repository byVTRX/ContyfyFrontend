import React, { Component, useEffect, useState } from "react";
import { Router, Switch, useHistory, Route } from "react-router-dom"
import Aside from "./AsideComponent"
import Header from "./HeaderComponent"
import Main from "./MainComponent"
import "./main.css"
import Logout from "./LogoutComponent";

import Notifications from "./NotificationsComponent"
import NotFound from "./NotFoundComponent"
import Profile from "./ProfileComponent"
import Settings from "./SettingsComponent"
import ProfileStats from "./ProfileStatsComponent"
import CreatePost from "./CreatePostComponent"

import { loadStripe } from "@stripe/stripe-js";

import firebase from "firebase/app"
import "firebase/auth"
import "firebase/functions"




import { createMuiTheme, makeStyles, ThemeProvider } from "@material-ui/core/styles"


import { CircularProgress, Divider, Grid, Hidden } from "@material-ui/core";
import DirectMessages from "./DirectmessagesComponent";


import TestComponent from "./TestComponent";
import Payment from "./Payment/PaymentComponent";
import AddCreditCard from "./Payment/AddCreditCardComponent"
import { Elements } from "@stripe/react-stripe-js";
import UserDashboard from "./Dashboard/Userdashboard";
import CreatorDashboard from "./Dashboard/CreatorDashboard";
import Dashboard from "./Dashboard/Dashboard";

import MobileHeader from "./MobileHeaderComponent";



const promise = loadStripe("pk_test_51IEzvPC6ZdO1ffhiS0GzbTF7mYO4QiByWMC3MTcyy4gRFc1ZbhqdnQfsHzsFEs7vLrnNU2tT7SBPWgg5q1JOjBNc003MOG2Yd6");

const useStyles = makeStyles(theme => ({
  main: {
    [theme.breakpoints.down('sm')]: {
      minWidth: 250,
      maxWidth: "100%"
    }
  },
  stats: {
    [theme.breakpoints.down('sm')]: {
      display: "none"
    }
  }
}))

export default class System extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
    console.log("INITIALIZED SYSTEM COMPONENT")

  }



  setData = () => {


    this.state = { loading: true }
    console.log("FETCHING USER DATA")

    const fetchData = async () => {
      const userInfo = firebase.auth().currentUser
      const userId = userInfo.uid;



      var getUser = firebase.functions().httpsCallable('getUser');

      getUser({ username: userId }).then((data) => {

        if (data.data.user.username === userId) {
          this.setState({
            user: data.data.user,
            loading: false,
          })
        } else {
          alert('Could not log user in')
        }
      })






    }



    fetchData();

  }

  componentDidMount() {
    this.setData();
  }









  renderModal() {


    const { user, loading } = this.state;
    const classes = makeStyles(theme => ({
      main: {

      },
      stats: {

      },
      aside: {

      }
    }))

    const darkTheme = createMuiTheme({
      palette: {
        type:'dark',
        primary: {
          main: '#e65100',
        },
        secondary: {
          main: '#512da8',
        },
        background: {
          default: '#424242',
        },
      },
    });

    if (user && !loading) {
      return (

        <div >

          <div style={{ display: "block" }}>
            <Hidden mdDown>
              <Header user={user} />
            </Hidden>
            <Hidden mdUp>
              <MobileHeader user={user} />
            </Hidden>


          </div>


          <Hidden mdDown>
            <div style={{ height: '70px' }} />
          </Hidden>


          <Grid container spacing={3} style={{ maxHeight: "100vh"}}>
            <Hidden mdDown>
              <Grid item md style={{ borderRight: "1px solid white", minHeight: "100%" }}>

                <Aside className={classes.aside} user={user} />

              </Grid>
            </Hidden>




            <Grid item className={classes.main} md={6} xs={12} id="maingrid" style={{ margin: 0, padding: 0, }}>
              <div style={{ height: "50px" }} />

              <ThemeProvider theme={darkTheme}>
                <Switch>
                  <Route exact path='/' component={() => <Main user={user} />} />
                  <Route path="/logout" component={() => <Logout user={user} />} />
                  <Route path="/notifications" component={() => <Notifications user={user} />} />
                  <Route exact path="/profile" component={() => <Profile givenUser={user} currentUser={user} />} />
                  <Route path="/profile/:username" component={() => <Profile currentUser={user} />} />
                  <Route path="/directmessages" component={() => <DirectMessages user={user} />} />
                  <Route path="/settings" component={() => <Settings user={user} />} />
                  <Route path="/create-post" component={() => <CreatePost user={user} />} />
                  <Route path="/dashboard" component={() => <Dashboard user={user} />} />
                  <Route path="/test" component={() => <TestComponent />} />
                  <Route path="/payment" component={() => <Elements stripe={promise}><Payment user={user} /></Elements>} />
                  <Route path="/addpayment" component={() => <Elements stripe={promise}><AddCreditCard user={user} /></Elements>} />
                  <Route path="/" component={NotFound} />

                </Switch>
              </ThemeProvider>

            </Grid>

            <Hidden mdDown>
              <Grid item className={classes.stats} style={{ borderLeft: "1px solid white", minHeight: "100%" }} md>
                <Switch>

                  {/*<Route path="/profile" component={() => <ProfileStats givenUser={user} className={classes.stats}/>}/>**/}
                </Switch>

              </Grid>

            </Hidden>



          </Grid>



        </div>









      )
    } else {
      return (

        <CircularProgress stlyle={{ alignSelf: "center", justifySelf: "center", display: "flex" }} />
      )
    }
  }









  render() {
    return (
      <div>
        {this.renderModal()}
      </div>
    )
  }


}