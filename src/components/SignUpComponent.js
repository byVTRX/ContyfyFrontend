import React, { Component, useState } from "react";


import {useHistory} from "react-router-dom"
import $ from "jquery";
import firebase from "firebase"
import "firebase/auth"
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { Button, CircularProgress } from "@material-ui/core";


export default function SignUp() {
    const history = useHistory()
    const [loading,setLoading] = useState(false);
        return (
            <form onSubmit={async (event) => {
                event.preventDefault();
                var user = {
                    email:$('#email').val(),
                    username:$('#username').val().toLowerCase(),
                    password:$('#password').val(),
                    displayName:$('#displayName').val(),
                    phoneNumber:$('#phone').val(),
                    birthdate:$('#birthdate').val(),
                    firstName:$('#firstName').val(),
                    lastName:$('#lastName').val(),
                    profilepicture:null,
                    /*address:{
                        firstName:$('#firstName').val(),
                        lastName:$('#lastName').val(),
                    }**/
                }
                signUp(user, history, setLoading);

                
            }
                
            }>
                <h3>Register</h3>
                <div className="form-group">
                    <label>Username</label>
                    <input type="text" className="form-control" placeholder="Username" id="username" style={{color:"black"}}/>
                </div>
                <div className="form-group">
                    <label>Display name</label>
                    <input type="text" className="form-control" placeholder="Display name" id="displayName" style={{color:"black"}}/>
                </div>
                <div className="form-group">
                    <label>First name</label>
                    <input type="text" className="form-control" placeholder="First name" id="firstName" style={{color:"black"}}/>
                </div>

                <div className="form-group">
                    <label>Last name</label>
                    <input type="text" className="form-control" placeholder="Last name" id="lastName" style={{color:"black"}}/>
                </div>

                <div className="form-group">
                    <label>Email</label>
                    <input type="email" className="form-control" placeholder="Enter email" id="email" style={{color:"black"}}/>
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" id="password" style={{color:"black"}}/>
                </div>

                <div className="form-group">
                    <label>Phone (+00123456789)</label>
                    <input type="text" className="form-control" placeholder="Enter phone" id="phone" style={{color:"black"}}/>
                </div>

                <div className="form-group">
                    <label>Birthdate</label>
                    <input type="date" className="form-control" placeholder="Enter birthdate" id="birthdate" style={{color:"black"}}/>
                </div>

                <Button type="submit" className="btn btn-dark btn-lg btn-block" startIcon={loading?<CircularProgress/>:<ArrowForwardIcon/>}>Register</Button>
                <p className="forgot-password text-right">
                    Already registered? <a href="/sign-in">Log in</a>
                </p>
            </form>
        );
    
}

async function signUp(userObj, history, setLoading){
    try {
        console.log("Signing up user through firebase auth")
        setLoading(true);
        var addUser = firebase.functions().httpsCallable('addUser');

        

        
            
            addUser(JSON.stringify(userObj)).then((res) => {
                
                    if(res.data.status === 200){

                        firebase.auth().signInWithEmailAndPassword(userObj.email, $('#password').val()).then(() => {
                            firebase.auth().currentUser.sendEmailVerification();
                            history.push("/")
                            
                        })
                    } else {
                        console.log("Error occured while creating user")
                        if(res.data.status === "UserAlreadyExists"){
                            alert('This username is already taken. Please select a different username')
                        } else {
                            alert(res.data.message)
                        }
                    }
                    
                
            })
            
            
       

            

            return false;

            
    } catch (error) {
        console.trace(error)
        return false
    }
    
      
}