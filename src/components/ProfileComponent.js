import React, { useState, useEffect, useReducer } from "react";
import {
  Button,
  List,
  ListItem,
  Divider,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Typography,
  CircularProgress,
  Card,
  CardContent,
  CardHeader,
  CardActions,
  IconButton,
  CardMedia,
  Collapse,
  MenuItem,
  Menu,
} from '@material-ui/core';

import MonetizationOnOutlinedIcon from '@material-ui/icons/MonetizationOnOutlined';
import MessageOutlinedIcon from '@material-ui/icons/MessageOutlined';
import NotificationsNoneOutlinedIcon from '@material-ui/icons/NotificationsNoneOutlined';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined';

import { makeStyles } from '@material-ui/core/styles';
import alertify from "alertifyjs";
import 'alertifyjs/build/css/alertify.css';

import firebase from "firebase"
import "firebase/auth"
import "firebase/storage"
import "firebase/firestore"

import "./main.css"

import PostList from "./PostList";

import {
  useParams,
  useHistory
} from "react-router-dom";

import _ from "lodash"


import NotFound from "./NotFoundComponent";
import Skeleton from "@material-ui/lab/Skeleton";


const SUBSCRIPTION = 'SUBSCRIPTION';
const INITIAL_QUERY = 'INITIAL_QUERY';
const ADDITIONAL_QUERY = 'ADDITIONAL_QUERY';

const reducer = (state, action) => {
  switch (action.type) {
    case INITIAL_QUERY:
      return action.posts;
    case ADDITIONAL_QUERY:
      return [...state, ...action.posts]
    case SUBSCRIPTION:
      return [action.post, ...state]
    default:
      return state;
  }
};
const useStyles = makeStyles(theme => ({
  listRoot: {
    width: '100%',
    wordBreak: 'break-all',
    overflow: 'none',
    listStyleType: "none",

  },
  alignCenter: {
    textAlign: 'center',
  },
  loader: {
    textAlign: 'center',
    paddingTop: 20,
  },
  maxWidth: {
    width: '100%',
  },
  listHeader: {
    position: 'sticky',
    top: 0,
    zIndex: 1200,
    backgroundColor: '#15202B',
    borderBottom: '1px solid #37444C',
    listStyleType: "none"
  },
  clickable: {
    cursor: 'pointer',
  },
  root: {
    width: "100%",

    borderRadius: 0,
    listStyleType: "none",

    background: "transparent",

  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: "white",
    width: theme.spacing(12),
    height: theme.spacing(12),
    alignSelf: "center",
    zIndex: "10",
    color:"black",
  },
  divider: {
    margin: '5px 0 0 ${theme.spacing(9)}px'
  },
  content: {


    display: "flex",
    justifyContent: "center",
    height: "auto",
    flexDirection: "column",
    alignItems: "center",
  },
  actions: {
    display: "flex",
    flexDirection: "row",
    margin: "0",
    padding: "0",
    justifyContent: "center",
    alignItems: "center"
  },
  settingsButton: {
    "&:hover":{
      background:"linear-gradient(90deg, rgba(255,146,1,1) 0%, rgba(251,34,34,1) 81%);",
    },
    
    border:"2px solid white",
    borderRadius:"10px",
   
  }

}));

export default function Profile({ givenUser, params, currentUser }) {
  const classes = useStyles();
  let { username } = useParams();
  const [posts, dispatch] = useReducer(reducer, []);
  const [nextToken, setNextToken] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [loading, setLoading] = useState(true)
  const [user, setUser] = useState(givenUser ? givenUser : null);
  const [isSubscribed, setSubscribed] = useState(null)
  const [price, setPrice] = useState(0);
  const [menuAnchor, setMenuAnchor] = useState(null);
  const history = useHistory();



  const getPosts = async (userName, type, nextToken = null) => {
    console.log('FETCHING POSTS')
    var fetchPosts = firebase.functions().httpsCallable('getPostsForUser');
    var res = await fetchPosts({ username: userName, requestor: firebase.auth().currentUser.uid })
    const data = res.data;

    if (data.posts) {
      dispatch({ type: type, posts: data.posts.map(post => { return { id: post.id, post: post.post } }) })
      setNextToken(null);
    }

    setIsLoading(false);
  }




  const getAdditionalPosts = () => {
    if (nextToken === null) return; //Reached the last page
    getPosts(ADDITIONAL_QUERY, nextToken);
  }

  const toggleSubscribe = async () => {





    if (isSubscribed) {
      console.log("UNSUBSCRIBING")

      firebase.firestore().collection('User').doc(firebase.auth().currentUser.uid).collection('Subscriptions').get().then(doc => {
        if (doc.exists) {
          var temp = doc.data();
          var subEntry = {};
          for (const sub of temp.subscriptions) {
            if (sub.username === username) {
              subEntry = sub;
            }
          }

          doc.ref.update({
            "subscriptions": firebase.firestore.FieldValue.arrayRemove(subEntry)
          })
        }
      })





      setSubscribed(false)
    } else {
      console.log("SUBSCRIBING")

      var subscribe = firebase.functions().httpsCallable('subscribe');
      var subscription = await subscribe({ username: username })
      console.log(subscription)
      if (subscription.data.sub_id) {
        setSubscribed(true)
      } else {
        alert(subscription.data.message)
      }

    }


  }




  useEffect(() => {



    if (givenUser) {
      setSubscribed(true)
      setLoading(false)
      const fetchPosts = async () => {
        

        getPosts(user.username, INITIAL_QUERY);
      }
      fetchPosts()
    } else {
      if (username) {

        const fetch = async () => {
          setLoading(true)
          var userRef = await firebase.functions().httpsCallable('getUser')
          var userData = (await userRef({ username })).data.user
          console.log(userData)

          setUser(userData)


          getPosts(userData.username, INITIAL_QUERY)



          var getPrice = await firebase.functions().httpsCallable('getSubscriptionPrice');
          var subscriptionPrice = await getPrice({ username });

          setPrice(subscriptionPrice.data.price)
          
          
          var subRef = await firebase.functions().httpsCallable('getSubscriptions')

          var subData = (await subRef({ username: firebase.auth().currentUser.uid })).data.subscriptions

          subData.map((sub) => {
            
            if (sub.username === username) {
              setSubscribed(true)
            }
          })

          setLoading(false)
        }


       
        fetch()
      }
    }







  }, []);

  const openMenu = (event) => {
    setMenuAnchor(event.currentTarget)
  }

  const handleCloseMenu = () => {
    setMenuAnchor(null);
  } 

  if (user) {

    return (<Card className={classes.root} style={{ marginBottom: "0px", marginTop: "-50px", color: "white" }} elevation={3}>

      <CardMedia
        className={classes.media}
        image={user.banner ? user.banner : "https://c.wallhere.com/photos/10/ec/simple_background-118406.jpg!d"}
        title="banner"
      />
      <Divider light variant="middle" />
      <CardHeader action={
        <div>
          <IconButton>
            <MonetizationOnOutlinedIcon style={{ fontSize: "20px" }} />
          </IconButton>

          <IconButton>
            <MessageOutlinedIcon style={{ fontSize: "20px" }} />
          </IconButton>

          <IconButton>
            <NotificationsNoneOutlinedIcon style={{ fontSize: "20px" }} />
          </IconButton>

          <IconButton aria-label="settings" aria-controls="menu" aria-haspopup="true" onClick={openMenu}>
            <MoreVertIcon />
          </IconButton>
          <Menu
            id="menu"
            anchorEl={menuAnchor}
            keepMounted
            open={Boolean(menuAnchor)}
            onClose={handleCloseMenu}
          >
            <MenuItem onClick={handleCloseMenu}>Report this profile</MenuItem>
            
          </Menu>
        </div>

      }>

      </CardHeader>
      <CardContent className={classes.content} >
        <Avatar className={classes.avatar} src={user.profilepicture && user.profilepicture} style={{ marginTop: "-130px" }}>
        </Avatar>
        <Typography variant="h4"  style={{ marginTop: "10px", color:"white" }}>
          {loading ? <Skeleton style={{width:"100px", height:"auto", marginLeft:"10px"}}/> : user.displayName}
        </Typography>
        <Typography variant="subtitle1" style={{color:"grey", display:"inline-flex"}} >
          @{loading ? <Skeleton style={{width:"50px", height:"auto", marginLeft:"10px"}}/> : user.username}
        </Typography>
        <div className="spacer" style={{ height: "20px" }}></div>
        
        <Typography variant="p" component="small" style={{ color:"white", textAlign:"center"}}>
          {user.verifiedStatus >= 2 && user.creatorSettings.bio}
        </Typography>
        <div className="spacer" style={{ height: "20px" }}></div>
        {givenUser ? 
        <Button className={classes.settingsButton + " btn"} style={{ alignSelf: "center", alignItems:"center", display:"flex", margin:"0" }} onClick={(e) => {
          e.preventDefault();
          history.push('/dashboard?settings')
        }}>
          <p style={{margin:0, textAlign: "center"}}>
          <SettingsOutlinedIcon style={{fontSize:"30px"}}/> Profile Settings
          </p>
          
        </Button> : 
        <Button class="btn btn-outline-primary" style={{ alignSelf: "center" }} onClick={(e) => {
          e.preventDefault()
          if (!isSubscribed) {
            alertify.defaults.transition = "slide";
            alertify.defaults.theme.ok = "btn btn-primary";
            alertify.defaults.theme.cancel = "btn btn-danger";
            alertify.defaults.theme.input = "form-control";
            alertify.confirm("Do you really want to subscribe?", `You will be charged $${price.unit_amount / 100} Until you cancel`, () => { toggleSubscribe() }, () => { alertify.error("You were not charged") })

          } else {
            toggleSubscribe();
          }
        }}>
          
          {loading ? <Skeleton style={{width:"300px"}}/> : isSubscribed ? <Typography variant="h5"  style={{ display: "inline-flex", flexDirection: "row", color:"white" }} >Unsubscribe</Typography> : <Typography variant="h5"  style={{ display: "inline-flex", flexDirection: "row", color:"white" }}>Subscribe for ${price.unit_amount/100}</Typography>}
         
        </Button>
        }
        



        <div className="spacer" style={{ height: "20px" }}></div>
        
      </CardContent>
      <CardActions className={classes.actions}>
        {user.instagram && <a className="btn" style={{ height: "36px", fill: "white" }} href={user.instagram} target="__blank">
          <svg xmlns="http://www.w3.org/2000/svg" width={20} height={20} viewBox="0 0 24 24"><path d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z" /></svg>
        </a>}

        
        {user.twitter && <a className="btn" style={{ height: "36px", fill: "white" }} href={user.twitter} target="__blank">
          <svg xmlns="http://www.w3.org/2000/svg" width={20} height={20} viewBox="0 0 24 24"><path d="M24 4.557c-.883.392-1.832.656-2.828.775 1.017-.609 1.798-1.574 2.165-2.724-.951.564-2.005.974-3.127 1.195-.897-.957-2.178-1.555-3.594-1.555-3.179 0-5.515 2.966-4.797 6.045-4.091-.205-7.719-2.165-10.148-5.144-1.29 2.213-.669 5.108 1.523 6.574-.806-.026-1.566-.247-2.229-.616-.054 2.281 1.581 4.415 3.949 4.89-.693.188-1.452.232-2.224.084.626 1.956 2.444 3.379 4.6 3.419-2.07 1.623-4.678 2.348-7.29 2.04 2.179 1.397 4.768 2.212 7.548 2.212 9.142 0 14.307-7.721 13.995-14.646.962-.695 1.797-1.562 2.457-2.549z" /></svg>
        </a>}

      </CardActions>
      <div className="spacer" style={{ height: "20px" }}></div>
      <Divider color="white" />

      {posts.length > 0 &&
        <PostList
          isLoading={isLoading}
          posts={posts}
          getAdditionalPosts={getAdditionalPosts}
          listHeaderTitle={'Feed'}
          user={user}

        />

      }




    </Card>
    )
  } else {
    return <NotFound />
  }

}