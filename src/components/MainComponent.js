import React, {useState, useEffect, useReducer} from "react";

import {useHistory} from "react-router-dom"
import "./main.css"
import "bootstrap/dist/css/bootstrap.min.css"



import PostList from "./PostList"


import _ from 'lodash';
import firebase from "firebase"

const SUBSCRIPTION = 'SUBSCRIPTION';
const INITIAL_QUERY = 'INITIAL_QUERY';
const ADDITIONAL_QUERY = 'ADDITIONAL_QUERY';

const reducer = (state, action) => {
    switch (action.type) {
      case INITIAL_QUERY:
        return action.posts;
      case ADDITIONAL_QUERY:
        return [...state, ...action.posts]
      case SUBSCRIPTION:
        return [action.post, ...state]
      default:
        return state;
    }
  };

export default function Main({user}) {
    const [posts, dispatch] = useReducer(reducer, []);
    const [nextToken, setNextToken] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [visiblePosts, setVisiblePosts] = useState([])

    
    

    var getPosts = async (type, nextToken =null) => {
        var fetchPosts = firebase.functions().httpsCallable('getPosts');
        const res = await fetchPosts(JSON.stringify({username: user.username,  pagination:nextToken})) 
        
        var posts = res.data.posts
        console.log(posts);
        if(posts && posts.length>0){
          dispatch({ type: type, posts: posts.map(post => { return {id:post.id, post:post.post}} )})
        
          setNextToken(posts.reverse()[0].id)
        } else {
          dispatch({type:type,posts:[]})
          setNextToken(null)
        }
        
        
        setIsLoading(false);
        
    }

    const getAdditionalPosts = () => {
        if (nextToken === null) return; //Reached the last page
        getPosts(ADDITIONAL_QUERY, nextToken);
      }

      useEffect(() => {
        console.log('init')
        const init = async () => {
          
    
          getPosts(INITIAL_QUERY);
        }
        
        init();
        
      }, []);

      useEffect(() => {
        
      })

      

      

      
        
        
        
        return(<div style={{margin:"10px 0"}}>
                
                
                
                
                
                {posts.length > 0 ? <PostList
                    isLoading={isLoading}
                    posts={posts}
                    getAdditionalPosts={getAdditionalPosts}
                    listHeaderTitle={'Feed'}
                    user={user}
                    showCategories={true}
                    /> : <div className="text-center">No Posts found</div>}
                
                
                
            </div>
        );
    
}










