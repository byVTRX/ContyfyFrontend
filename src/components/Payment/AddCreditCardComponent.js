
import { Button, CircularProgress, Divider, List, ListItem, ListItemText, makeStyles } from "@material-ui/core";
import React, { useEffect, useReducer, useState } from "react";
import EditIcon from '@material-ui/icons/Edit';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { CardElement, useElements, useStripe } from "@stripe/react-stripe-js";
import Payment from "@material-ui/icons/Payment";

import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import ErrorIcon from '@material-ui/icons/Error';

import firebase from "firebase/app";
import "firebase/functions";

import alertifyjs from "alertifyjs"

const useStyles = makeStyles((theme) => ({
    addCard: {
        width: "400px"
    },
    content: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
    }
}))

const cardStyle = {
    style: {
        base: {

            color: "#32325d",
            fontFamily: 'Roboto, sans-serif',
            fontSmoothing: "antialiased",
            fontSize: "16px",
            "::placeholder": {
                color: "#32325d"
            }
        },
        invalid: {
            color: "#fa755a",
            iconColor: "#fa755a"
        }
    }
};

export default function AddCreditCard({ user }) {
    const classes = useStyles();
    const stripe = useStripe();
    const elements = useElements();
    const [loading, setLoading] = useState("initial");
    const [error, setError] = useState("unknown")




    return (<div className={classes.content}>
        <Payment style={{ color: "white", fontSize: "40px" }} />
        <h3 style={{ color: "white" }}>Add Credit card</h3>

        <div className={classes.addCard}>
            <CardElement id="card-element" options={cardStyle} onChange={(e) => { e.error && setError(e.error.message) }} />

            {loading === "initial" &&

                <div>
                    <Button onClick={async () => {
                        setLoading("progressing")


                        var cardElement = elements.getElement(CardElement);
                        
                        

                        var token = await stripe.createToken(cardElement);
                        
                        if(!token.token){
                            setLoading("error")
                        } else {
                            
                            var setupCredit = await firebase.functions().httpsCallable('setupCreditCard');
                            var res = await setupCredit({ username: user.username, stripeId: user.stripeId, card:token.token.id  }).then((res) => {
                                if (res.data.status === 500) {
    
                                    setError(res.data.message)
                                    setLoading("error")
                                } else {
                                    setLoading("success")
                                }
    
    
    
                            })
                        }
                        
                       
                    }} id="card_submit_button">Submit</Button>
                    <small className="text-muted">By Submitting your credit card data you consent to let Contyfy Network UG and Stripe GmbH charge your credit card on your behalf. You can revoke your consent at any time by removing your credit card under the <a href="/payment">Payment section</a> </small>
                </div>
            }

            {loading === "progressing" &&
                <div style={{ marginTop: "30px" }}>
                    <CircularProgress />
                    <h3 style={{ color: "white", fontSize: "24px" }}>Your payment method is beeing added</h3>
                </div>
            }

            {loading === "success" &&
                <div style={{ marginTop: "30px" }}>
                    <CheckCircleOutlineIcon style={{ color: "green", fontSize: "30px" }} />
                    <h3 style={{ color: "white", fontSize: "24px" }}>Your Credit Card has been added successfully. View it under the <a href="/payment">Payment</a> section.</h3>
                </div>
            }

            {loading === "error" &&
                <div style={{ marginTop: "30px" }}>
                    <ErrorIcon style={{ color: "red", fontSize: "30px" }} />
                    <h3 style={{ color: "white", fontSize: "24px" }}>Your Credit Card could not be added to your account. </h3>
                    <small className="text-muted">Refresh the page to start over</small>
                </div>
            }




        </div>
    </div>)
}



