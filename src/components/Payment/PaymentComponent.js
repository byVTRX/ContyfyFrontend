import React, { useEffect, useState } from "react";

import firebase from "firebase"
import "firebase/functions"
import { Backdrop, Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, IconButton, List, ListItem, ListItemAvatar, makeStyles, TextField } from "@material-ui/core";
import ClearIcon from '@material-ui/icons/Clear';
import AddIcon from '@material-ui/icons/Add';
import alertifyjs from "alertifyjs";
import TransactionList from "./TransactionlistComponent";
import BillingAddress from "./BillingAddressComponent";

import PaymentIcon from '@material-ui/icons/Payment';
import { CardElement, useElements, useStripe, } from "@stripe/react-stripe-js";
import { Icon, InlineIcon } from "@iconify/react";
import paypalIcon from "@iconify-icons/mdi/paypal";
import { useHistory } from 'react-router';

import AddCircleIcon from '@material-ui/icons/AddCircle';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import paymentLogos from "../paymentlogos.png"

const useStyles = makeStyles((theme) => ({
    [theme.breakpoints.down('sm')]: {
        paymentItem: {
            width: "200px",
            height: "300px",
        },
    },
    [theme.breakpoints.up('sm')]: {
        paymentItem: {
            width: "200px",
            height: "150px",
        },

    },
    paymentItem: {
        boxShadow: "0 6px 6px rgba(0,0,0,0.4)",
        display: "flex",
        flexDirection: "column",
        borderRadius:"15px",
        alignItems: "center",
        margin: "10px",

    },
    paymentList: {
        padding: "20px",
        display: "grid",
        gridTemplateColumns: "repeat(4, 1fr)",
        gridAutoRows:"minmax(150px, auto)",
        gridAutoFlow:"row",
        justifyContent: "center",
        borderBottom:"1px solid white",
        [theme.breakpoints.down('md')]:{
            display:"flex",
            flexDirection:"column",
        }
    },

    widgets: {
        padding: "20px",
        display: "grid",
        gridTemplateColumns: "repeat(2, 1fr)",
        gridAutoRows:"minmax(150px, auto)",
        gridAutoFlow:"row",
        justifyContent: "center",
        borderBottom:"1px solid white",
        [theme.breakpoints.down('md')]:{
            display:"flex",
            flexDirection:"column",
        }
    },
    
    transactionList:{
        gridColumnStart:"2",
        gridColumnEnd:"3",
        gridRowStart:"1",
        gridRowEnd:"3",
        boxShadow:"0 6px 6px rgba(0,0,0,0.4)",
        padding:"5px",
        margin:"10px",

    },
    BillingAddress:{
       
        gridColumnStart:"1",
        gridColumnEnd:"2",
        gridRowStart:"1",
        gridRowEnd:"3",
        
        boxShadow:"0 6px 6px rgba(0,0,0,0.4)",
        padding:"10px",
        margin:"10px",
        

    },
    cardIcon: {
        filter: "invert(1)"
    },
    backdrop: {
        zIndex: 1000,
        color: '#fff',
    },
}))

export default function Payment({ user }) {


    alertifyjs.defaults.transition = "slide";
    alertifyjs.defaults.theme.ok = "btn btn-primary";
    alertifyjs.defaults.theme.cancel = "btn btn-danger";
    alertifyjs.defaults.theme.input = "form-control";

    const classes = useStyles();
    const [paymentMethods, setPaymentMethods] = useState([])
    const [transactions, setTransactions] = useState([])
    const [address, setAddress] = useState({})
    const [loading, setLoading] = useState(false);
    const [chooseAddPaymentOpen, setChooseAddPaymentOpen] = useState(false);
    const [creditCardDialogOpen, setCreditCardDialogOpen] = useState(false);
    const [clientSecret, setClientSecret] = useState(null);

    const history = useHistory();

    const stripe = useStripe();
    const elements = useElements();

    var addPaymentValue = "";

    const fetch = async () => {
        var getPaymentMethods = await firebase.functions().httpsCallable('getPaymentMethods');
        setLoading(true)
        var paymentData = await getPaymentMethods({ stripeId: user.stripeId, uid: user.username });
        console.log(paymentData.data)
        setPaymentMethods(paymentData.data)




        var getTransactions = await firebase.functions().httpsCallable('getTransactions');
        var transactions = await getTransactions({stripeId: user.stripeId, username: user.username});
        console.log(transactions.data)
        
        setTransactions(transactions.data ? transactions.data.sort((a,b) => b.created - a.created) : null)

        var getAddress = await firebase.functions().httpsCallable('getAddress');
        var addressData = await getAddress({stripeId: user.stripeId, username: user.username});
        console.log(addressData.data)
        setAddress(addressData.data ? addressData.data : null)





        setLoading(false);
    }


    const availablePaymentMethods = [
        {name:"paypal"},
        {name:"credit card"}
    ]


    const cardStyle = {
        style: {
          base: {
            color: "#32325d",
            fontFamily: 'Roboto, sans-serif',
            fontSmoothing: "antialiased",
            fontSize: "16px",
            "::placeholder": {
              color: "#32325d"
            }
          },
          invalid: {
            color: "#fa755a",
            iconColor: "#fa755a"
          }
        }
      };

    useEffect(() => {




        fetch()
    }, [])

    const handleClickOpen = () => {
        setChooseAddPaymentOpen(true);
      };
    
      const handleClose = () => {
        setChooseAddPaymentOpen(false);
      };

      const openAddDialog = async (method) => {
          if(method === "credit card"){
            setChooseAddPaymentOpen(false)
            
        
            setCreditCardDialogOpen(true)
          }
      }
    
      const closeCreditCardDialog = () => {
        setCreditCardDialogOpen(false);
    }

    const handleConfirmSetupIntent = async () => {
        const addPayment = await stripe.createPaymentMethod({
            type:'card',
            card:elements.getElement(CardElement),
            
        }).then(async (result) => {
            console.log(result)
            var setupCredit = await firebase.functions().httpsCallable('setupCreditCard');
            var res = await setupCredit({username:user.username, stripeId:user.stripeId, setupId:result.paymentMethod.id}).then((res) =>{
                if(res.error){
                    alertifyjs.error(res.error)
                } 
            })

            setCreditCardDialogOpen(false)
              fetch()
              if(result.error){
                alertifyjs.error(result.error)
            } else {
                alertifyjs.success('PaymentMethod has been added successfully')
            }
        })
    }
    

    if (loading) {
        return (<>
            
            <CircularProgress />
        </>)
    } else {
        return (<>
            
            <h3 style={{textAlign:"left", color:"white", marginLeft:"30px"}}><a style={{cursor:"pointer"}} onClick={() => {history.goBack()}}><ArrowBackIcon/></a>Payment</h3>

            <div className={classes.widgets}>
            <div className={classes.BillingAddress}>
            <BillingAddress addressData={address} />
            </div>
            
            <div className={classes.transactionList}>
            <TransactionList transactions={transactions} />
            </div>
            </div>

            <h3 style={{textAlign:"left", color:"white", margin:"20px", marginLeft:"30px"}}>Payment methods</h3>
            <div className={classes.paymentList}>
            
           
            <PaymentList paymentMethods={paymentMethods} user={user} fetch={fetch} />
            
            </div>
            <div style={{padding:"30px"}}>
            <img src={paymentLogos} width="300px"/>
            <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={chooseAddPaymentOpen}>
      <DialogTitle id="simple-dialog-title">Choose payment method</DialogTitle>
      <List>
        {availablePaymentMethods.map((paymentMethod) => {
            return <ListItem button onClick={() => {openAddDialog(paymentMethod.name)}}>
                <ListItemAvatar><PaymentIcon/></ListItemAvatar>
                {paymentMethod.name}
                </ListItem>
        })}

        
      </List>
    </Dialog>

    <Dialog onClose={closeCreditCardDialog} style={{minWidth:"200px"}} aria-labelledby="simple-dialog-title" open={creditCardDialogOpen}>
      <DialogTitle id="simple-dialog-title">Add a new credit card</DialogTitle>
      <form id="credit_card_form">
      <CardElement id="card-element" options={cardStyle} onChange={() => {}} />
      <Button onClick={async () => {
          
          handleConfirmSetupIntent()
      }} id="card_submit_button">Submit</Button>
      </form>
     
    </Dialog>
            </div>
            
           
               



        </>)
    }

}





function PaymentList({ paymentMethods, fetch, user }) {
    const classes = useStyles();

    return (<>
        {paymentMethods.length > 0 && paymentMethods.map((method) => <PaymentItem paymentMethod={method} fetch={fetch} user={user} />)}
        <a href="/addpayment">
        <AddPaymentItem/>
        </a>
        
        <a target="__blank" href={`https://www.sandbox.paypal.com/connect?flowEntry=static&client_id=AXZbqDIBZ70YoCeiidW8UZwpMp1fSg9to4JDS40Aeqjw2CRufAe03CmSZOc48HlKfC_c2YCMfcvhgBUx&scope=openid email profile address&redirect_uri=https%3A%2F%2Fcontyfy.com&paypalconnected=true`}>
        <AddPayPalItem/>
        </a>
        </>)
}



function PaymentItem({ paymentMethod, fetch, user }) {
    const classes = useStyles();
    const [backdropOpen, setBackdropOpen] = useState(false)
    return (<div className={classes.paymentItem} key={paymentMethod.id}>
        {paymentMethod.card.brand === "visa" ? <img alt="Visa" src="https://img.icons8.com/ios/50/000000/visa.png" className={classes.cardIcon} /> : <img className={classes.cardIcon} alt="Mastercard" src="https://img.icons8.com/ios-filled/50/000000/mastercard.png" />}
        <span>**** **** **** {paymentMethod.card.last4}</span>
        <span>{paymentMethod.card.exp_month} / {paymentMethod.card.exp_year}</span>
        <span><ClearIcon style={{ fontSize: "30px", marginTop: "20px", borderRadius: "30px", cursor: "pointer" }} onClick={(e) => {
            e.preventDefault();
            alertifyjs.confirm('Remove payment method', "Are you sure you want to remove the payment Method " + paymentMethod.card.brand + " " + paymentMethod.card.last4 + "?\n WARNING: THIS WILL NOT CANCEL ANY ACTIVE SUBSCRIPTION ON YOUR ACCOUNT. YOU WILL STILL BE SENT AN INVOICE AT THE END OF THE BILLING CYCLE!", async () => {
                setBackdropOpen(true)
                var removePaymentMethod = firebase.functions().httpsCallable("removePayment");

                const res = await removePaymentMethod({ paymentId: paymentMethod.id, username: user.username, stripeId:user.stripeId });

                setBackdropOpen(false)
                if (res.data.status === 200) {
                    alertifyjs.success(res.data.message)
                } else {
                    alertifyjs.error(res.data.message)
                }
                fetch()
            }, () => { })
        }} className="bg-danger" /></span>
        <Backdrop open={backdropOpen} className={classes.backdrop}>
            <CircularProgress />
        </Backdrop>
    </div>)
}


function AddPaymentItem({ user }) {
    const classes = useStyles();
    
    return (<div className={classes.paymentItem} key="add_payment_method" style={{background:"linear-gradient(90deg, rgba(255,146,1,1) 0%, rgba(251,34,34,1) 81%)", color:"white"}}> 
        <PaymentIcon style={{fontSize:"50px", marginTop:"5px"}} />
        <h4>Add Credit Card</h4>
        <AddCircleIcon style={{fontSize:"30px"}}/>
        
    </div>)
}

function AddPayPalItem({ user }) {
    const classes = useStyles();
    
    return (<div className={classes.paymentItem} style={{background:"#4d13d1", color:"white"}} key="add_payment_method">
        <Icon icon={paypalIcon} width="50" height="50" style={{marginTop:"10px"}} />
        <h4>Add PayPal</h4>
        <AddCircleIcon style={{fontSize:"30px"}}/>
        
    </div>)
}