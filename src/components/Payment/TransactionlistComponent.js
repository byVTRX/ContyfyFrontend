
import { Button, Divider, IconButton, List, ListItem, makeStyles, Menu, MenuItem, Typography } from "@material-ui/core";
import React, { useEffect, useReducer, useState } from "react";

import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
const useStyles = makeStyles((theme) => ({
    transactionList:{
        height:"100%",
        width:"100%",
        display:"flex",
        flexDirection:"column",
        alignItems:"center",
        color:"white",
        position:"relative",
        
        
    },
    transaction:{
        height:"50px",
        width:"80%",
        display:"flex",
        color:"white",
        backgroundColor:"transparent",
        flexDirection:"row",
        fontSize:"10px",
        textAlign:"center",
        alignItems:"center",
        justifyContent:"space-between"
    },
    item:{
        display:"flex",
        flexDirection:"row",
        justifyContent:"space-between"
    }
}))


export default function TransactionList({transactions}) {
    const classes = useStyles();
    
    const [menuOpen, setMenuOpen] = useState(null);
    
   
    const handleClose = () => {
        setMenuOpen(null)
    }


    
    
    
    return (<>
    <div className={classes.transactionList}>
    Last Transactions <br/>
        <br/>
    
        
    {transactions.length > 0 ? <List style={{width:"100%", justifyContent:"center"}}>{transactions.map(transaction => transaction.charges.data[0] && <Transaction key={transaction.id} transaction={transaction}/>)}</List> : 
    <div>
    <h3>No Recent Transactions</h3>
    <Button className="btn btn-success">View all transactions</Button>
    </div>
    }
    
    
    </div>
    
    </>)
}



function Transaction({transaction}) {
    const classes = useStyles();
    const charge = transaction.charges.data[0];
    const [menuOpen, setMenuOpen] = useState(null);
    if(charge){
        var date = new Date(charge.created*1000)
    }
    

    const handleClose = () => {
        setMenuOpen(null)
    }

    return(<ListItem divider className={classes.item} style={{width:"100%", height:"40px"}}>
    
   
        
       {charge &&        <><Typography style={{fontSize:"12px"}}>${charge.amount/100} - {charge.payment_method_details.card.brand.toUpperCase() + " - " + charge.payment_method_details.card.last4 + " - " + (date.getMonth()+1) + "/" + date.getDate() + "/" + date.getFullYear()}{charge.refunded ? <b style={{color:"orange"}}> - REFUNDED</b> : <b style={{color:"green"}}> - PAID</b>}</Typography>  <a className="text-primary" style={{}}   onClick={(e) => {e.preventDefault(); setMenuOpen(e.currentTarget)}}><MoreHorizIcon style={{cursor:"pointer"}}/></a></>} 

        
       
    
    <Menu
  id="simple-menu"
  anchorEl={menuOpen}
  keepMounted
  open={Boolean(menuOpen)}
  onClose={handleClose}
>
  <MenuItem onClick={() => {handleClose(); window.open(charge.receipt_url, "__blank")}}>View More</MenuItem>
  
</Menu>
    </ListItem>)
}