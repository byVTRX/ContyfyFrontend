const functions = require("firebase-functions");
const cors = require('cors')({ origin: true });
const admin = require('firebase-admin');
const crypto = require('crypto');
admin.initializeApp();
const _ = require('lodash');
const stripe = require('stripe')('sk_test_51IEzvPC6ZdO1ffhipXqrm61PUGTDz9cGRaBSbEkx50kmHiDanEQNN3I56FCmcGoSWnCwE0RmyJS6OnrAEgS5iqgS00lX6d4TIh')



exports.getCreatorDashboardData = functions.https.onCall(async(data, context) => {
    /*const uid = context.auth.uid;
    const userRef = await admin.firestore().doc(`User/${uid}`).get();
    const userdata = userRef.data();
    

    if(userdata.verifiedStatus >= 2){
        const creatorData = {}
          const balance = await stripe.balance.retrieve({
            stripeAccount: userdata.creatorSettings.stripeID
          });

        const payouts = await stripe.payouts.list({
            limit:5
        },{
            stripeAccount:userdata.creatorSettings.stripeID
        });

        creatorData.payouts = payouts.data;
          
        creatorData.availableBalance = balance.available[0].amount
        creatorData.pendingBalance = balance.pending[0].amount
        creatorData.currency = balance.available[0].currency

        console.log(creatorData)

        return({status:200,creatorDashboard:creatorData})
    } else {
        return {status:403,message:"The requested user is not a creator"}
    }*/

    return({status:400})


})

exports.unsubscribe = functions.https.onCall(async(data,context) => {
    
})

exports.requestTipPayment = functions.https.onCall(async(data,context) => {
    const username = context.auth.uid;
    const creator = data.creatorId;
    const customerId = data.customerId;
    const amount = data.amount;
    const postId = data.postId;

    const postRef = await admin.firestore().collection('User').doc(creator).collection('Posts').doc(postId).get();
    const post = await postRef.data();

    const paymentIntent = await stripe.paymentIntents.create({
        amount,
        currency:'usd',
        payment_method_types:['card'],
        customer:customerId
    });

    const paymentMethods = await stripe.paymentMethods.list({
        customer:customerId,
        type:'card',
    })

    const paymentId = paymentIntent.id;
    const tipAmount = paymentIntent.amount;
    const currency = paymentIntent.currency;
    console.log(paymentIntent)
    console.log({amount:tipAmount,currency,creator,})

    return({status:200, tipData:{paymentId,amount:tipAmount,currency, creator, username,paymentMethods:paymentMethods.data}})

})

exports.confirmTipPayment = functions.https.onCall(async (data, context) => {
    
    const email = data.email;
    const paymentId = data.paymentId;
    const payment_method = data.paymentMethod;
    const uid = context.auth.uid;
    const creator = data.creatorId;
    const timestamp = Math.floor(Date.now()/1000);
    const postId = data.postId;

    try {
        const paymentIntent = await stripe.paymentIntents.confirm(paymentId, {
            payment_method,
            receipt_email: email
        });
        console.log(paymentIntent)

        const postRef = await admin.firestore().collection('User').doc(creator).collection('Posts').doc(postId).get();
        const post = await postRef.data();

        const userRef = await admin.firestore().collection('User').doc(uid).get();
        const userData = await userRef.data();

        const notification = {
            type:"tip",
            issuer:uid,
            recepient:creator,
            image:userData.profilepicture,
            content:" has tipped $" + paymentIntent.amount/100 +" to your post \"" + post.title + "\"",
            timestamp:timestamp,
        }

        console.log(notification);

        const res = await createNotification(notification)

        return({status:200, paymentIntent})
    } catch (error) {
        return({status:500, error})
    }
    

})

// exports.onPostChange = functions.firestore.document('Users/{userId}/Posts/{postId}').onWrite((change, context) => {


//     const stats = {
//         likes:0,
//         subscriber:0,
//         videos:0,
//         photos:0,
//         rank:0,
//         revenue:0,

//     }



//     admin.firestore().collection('Stats').doc(context.params.userId).collection().set()
// })


exports.setupCreditCard = functions.https.onCall(async (data, context) => {
    const uid = context.auth.uid;
    const stripeId = data.stripeId;
    const username = data.username;
    const card = data.card;

    if (username === uid || uid === "marvinbuth") {

        const setup = await stripe.customers.createSource(stripeId, { source: card }).then((res) => {
            if (res.severity === "ERROR") {
                return ({ status: 500, message: res.message })
            } else {
                return ({ status: 200, message: res.id })
            }
        })
        return (setup)
    } else {
        return ({ status: "UserNotAuthorized", message: "The requesting user is not authorized to setup a payment method for the requested user" })
    }
})

function generateAccountLink(accountID, origin) {
    console.log("CREATING ACCOUNT LINK URL WITH " + origin + " " + accountID)
    return stripe.accountLinks.create({
        type: "account_onboarding",
        account: accountID,
        refresh_url: `${origin}/onboard_refresh`,
        return_url: `${origin}/dashboard`,
    }).then((link) => {
        console.log(link)
        return link.url
    });
}


exports.stripeWebhook = functions.https.onCall(async (data, context) => {
    const event = data;

    switch (event.type) {
        case 'account.external_account.created':
            const accountID = event.id;
        default:
            console.log('UNHANDLED STRIPE WEBHOOK ' + event)
            break;
    }
})

exports.refreshOnboarding = functions.https.onCall(async (data, context) => {
    const uid = context.auth.uid;
    const userRef = await admin.firestore().doc(`User/${uid}`).get();
    const userdata = userRef.data();

    const origin = (context.rawRequest.secure ? "https://" : "http://") + context.rawRequest.hostname;

    const accID = userdata.creatorSettings.stripeID;
    const link = await generateAccountLink(accID, origin)
    context.rawRequest.res.redirect(link.url)
})

exports.likePost = functions.https.onCall(async (data, context) => {
    const uid = context.auth.uid;
    const username = data.postUser;

    const likeUser = await admin.firestore().collection('User').doc(uid).get();
    const userData = likeUser.data();
    const doc = await admin.firestore().collection('User').doc(username).collection('Posts').doc(data.postId);
    const timestamp = Math.floor(Date.now()/1000);
        
        
    
    const newLike = {username:context.auth.uid};
    console.log(newLike)
    const res = doc.update({
        "likes":admin.firestore.FieldValue.arrayUnion(newLike)
     })

    if(newLike){
        const notification = {
            type:"like",
            issuer:uid,
            recepient:username,
            image:userData.profilepicture,
            content:" has liked your post \"" + (await doc.get()).data().title + "\"",
            timestamp:timestamp,
        }

        const res = await createNotification(notification)
        return {status:200, message:"Post with id " + data.postID + " was liked"}
    } else {
        return {status:500, message:"Something went wrong while trying to like this post. Please try again later"}
    }
})

exports.unLikePost = functions.https.onCall(async (data, context) => {
    const doc = await admin.firestore().collection('User').doc(data.creatorId).collection('Posts').doc(data.postId);
    
        
    const liked = {username:context.auth.uid};
    console.log(liked)
    console.log(data.postId)
        
   
    const res = await doc.update({
        "likes":admin.firestore.FieldValue.arrayRemove(liked)
     }).then(() => {
        return {status:200, message:"Post with id " + liked.postID + " was unliked"}
     }).catch(() => {
        return {status:500, message:"Something went wrong while trying to like this post. Please try again later"}
     })

     return res;
})




exports.approveCreator = functions.https.onCall(async (data, context) => {

    try {
        const account = await stripe.accounts.create({ type: "express" });
        console.log('CREATED CONNECTED ACCOUNT ' + account.id)


        const origin = (context.rawRequest.secure ? "https://" : "http://") + context.rawRequest.hostname;
        console.log("ORIGIN: " + origin)

        const accountLinkURL = await generateAccountLink(account.id, origin);
        console.log(accountLinkURL)



        stripe.products.create({ name: context.auth.uid }).then(async (res) => {
            const price = await stripe.prices.create({
                unit_amount: 999,
                currency: 'usd',
                recurring: { interval: 'month' },
                product: res.id
            })
            admin.firestore().doc(`User/${context.auth.uid}`).update({
                creatorSettings: {
                    stripeID: account.id,
                    subscriptionpriceID: price.id,
                    productID: res.id,
                },
                verifiedStatus: 2,
            })
        })

        return ({ status: 200, url: accountLinkURL });
    } catch (err) {

        return ({ status: 500, message: err.message })
    }

})

exports.handleStripeWebhooks = functions.https.onCall(async (data, context) => {

})


exports.changePrice = functions.https.onCall(async (data, context) => {
    const uid = data.uid;
    const price_amount = data.price

    if (context.auth.uid === uid || context.auth.uid === "marvinbuth" || context.auth.uid === "ceo") {
        var price = null;
        try {
            var currentPrice = await stripe.prices.list({ product: uid })
            var priceID = currentPrice.data[0].id;

            price = await stripe.prices.delete(priceID, {
                unit_amount: price_amount,
            })
        } catch (error) {
            return ({ status: "PriceUpdateError", message: error })
        }
    } else {
        return ({ status: "AuthenticationError", message: uid + " is not permitted to perform this action" })
    }
})

exports.addUser = functions.https.onCall(async (data, context) => {
    const user = JSON.parse(data)




    return admin.auth().createUser({
        email: user.email,
        emailVerified: false,
        phoneNumber: user.phoneNumber,
        password: user.password,
        displayName: user.displayName,
        uid: user.username.toLowerCase(),
        disabled: false,


    }).then(async () => {


        /*const stripeUser = await stripe.customers.create({
            email: user.email,
            phone: user.phoneNumber,
            name: user.firstName + " " + user.lastName,

        })**/

        user.password = null;
        //user.stripeId = stripeUser.id;
        user.stripeId = "cus_JkV78llLGcW0tA";
        user.creatorSettings = {
            productID:"prod_JkV8oW5ERjtIeC", 
            stripeID:"acct_1J707v2ErNXiIqya",
            subscriptionpriceID:"price_1J707xC6ZdO1ffhiUj6poaBC",
        }
        user.verifiedStatus = 0;
        var dbUser = await admin.firestore().collection('User').doc(user.username.toLowerCase()).get()
        if (dbUser.exists) {
            return ({ status: "UserAlreadyExists", message: "Username is already taken" })
        } else {
            admin.firestore().collection('User').doc(user.username.toLowerCase()).set(user)
            return ({ status: 200, message: "User was successfully created" })
        }


    }).catch((e) => {
        console.error("ERROR CREATING USER " + e)
        return ({ status: "AuthenticationError", message: (e.errorInfo.message).replace('uid', 'username') })


    })







})

exports.getUserSettings = functions.https.onCall(async (data, context) => {
    const username = context.auth.uid;
    var user = {};
    var userRef = await admin.firestore().doc(`User/${username}`).get()
    if (userRef.exists) {
        var userData = userRef.data();
        user = {
            email: userData.email,
            phoneNumber: userData.phoneNumber,
            firstName: userData.firstName,
            lastName: userData.lastName,
            profilepicture: userData.profilepicture || null,
            username: userData.username,
            displayName: userData.displayName,
            birthdate: userData.birthdate,
            verifiedStatus: userData.verifiedStatus,
            Notifications: null,
            banner: userData.banner || null,
            instagram: userData.instagram || null,
            twitter: userData.twitter || null,

        }

        if (user.verifiedStatus >= 2) {
            user.creatorSettings = {
                subscriptionpriceID: userData.creatorSettings.subscriptionpriceID || null,
                payoutSchedule: userData.creatorSettings.payoutSchedule || "monthly",
                messagesFrom: userData.creatorSettings.messagesFrom || "everyone",
                bio: userData.creatorSettings.bio || null,
            }
        }
        return { user }
    } else {
        return { status: "UserNotFound", message: "The requested User was not found or does not exist" }
    }
})


exports.updateSettings = functions.https.onCall(async (data, context) => {
    const username = context.auth.uid;
    const settings = data;
    console.log(settings)
    const res = await admin.firestore().doc(`User/${username}`).update(settings);
    if (res) {
        return ({ status: 200, message: "New Settings have been saved.", settings })
    } else {
        return ({ status: 500, message: "An error occured while trying to update the users settings. Please try again later." })
    }
})

exports.getTransactions = functions.https.onCall(async (data, context) => {
    const cusIdStripe = data.stripeId;
    const username = data.username;
    const uid = context.auth.uid;

    console.log("uid:" + uid + " username:" + username)

    if (username === uid || uid === "marvinbuth") {
        const paymentList = await stripe.paymentIntents.list({
            limit: 5,
            customer: cusIdStripe,
        })

        return (paymentList.data.length > 0 ? paymentList.data : [])
    } else {
        return ({ status: "UserNotAuthorized", message: "The requesting user is not authorized to see the requested users last transactions" })
    }
})

exports.getAddress = functions.https.onCall(async (data, context) => {
    const cusIdStripe = data.stripeId;
    const username = data.username;
    const uid = context.auth.uid;


    if (username === uid || uid === "marvinbuth") {
        const customer = await stripe.customers.retrieve(cusIdStripe)

        return (customer.address ? customer.address : null)
    } else {
        return ({ status: "UserNotAuthorized", message: "The requesting user is not authorized to see the requested users address" })
    }
})

exports.getUser = functions.https.onCall(async (data, context) => {

    var obj = data

    const username = obj.username;

    console.log(username);
    var ref = await admin.firestore().collection('User').doc(username).get()
    var user = ref.data()
    if (ref.exists) {

        if (user.username === context.auth.uid) {
            return { status:200, user: user }
        } else {
            return {
                status:200,
                user: {
                    displayName: user.displayName,
                    username: user.username,
                    profilepicture: user.profilepicture,
                    bio: user.bio,
                    banner: user.banner
                }
            }
        }

    } else {
        return { error: "User does not exist" }
    }




})

exports.removePayment = functions.https.onCall(async (data, context) => {
    const username = data.username;
    const uid = context.auth.uid;
    const paymentId = data.paymentId;
    const stripeId = data.stripeId;

    if (username === uid) {
        const paymentMethod = await stripe.customers.deleteSource(
            stripeId,
            paymentId
        );

        return (paymentMethod ? { status: 200, message: "Payment method was successfully removed from " + uid } : { status: 403, message: "There has been an error while removing the payment method. Please try again or contact support." })
    } else {
        return ({ status: "AuthenticationError", message: uid + " is not permitted to perform this action" })
    }
})


exports.getPaymentMethods = functions.https.onCall(async (data, context) => {
    var stripeId = data.stripeId;
    var username = data.uid;
    var uid = context.auth.uid;

    if (uid === username) {
        const paymentMethods = await stripe.paymentMethods.list({
            customer: stripeId,
            type: 'card',
        });

        return (paymentMethods.data)
    } else {
        return ({ status: "UserNotAuthorized", message: "The requesting user is not authorized to see the requested users payment methods" })
    }
})


exports.getPosts = functions.https.onCall(async (data, context) => {

    var obj = JSON.parse(data)

    const username = context.auth.uid;
    const pagination = obj.pagination;
    console.log('DATA SUBMITTED: ' + username, pagination)
    //CHECK IF USER IS ALLOWED TO VIEW POSTS

    var posts = []


    const subscriptionSnapshot = await admin.firestore().collection('User').doc(username).collection('Subscriptions').get();
    var subscriptions = [];
    subscriptionSnapshot.forEach(subscription => {
        subscriptions.push(subscription.data())
    })
    console.log(subscriptions);

    if (subscriptions.length > 0) {
        for (const sub of subscriptions) {
            const snapshot = pagination ? await admin.firestore().collection('User').doc(sub.username).collection('Posts').orderBy('timestamp', 'desc').startAfter(pagination).get() : await admin.firestore().collection('User').doc(sub.username).collection('Posts').orderBy('timestamp', 'desc').get()
            for (const doc of snapshot.docs) {
                console.log("ADDING TO POSTS ARRAY: " + doc.id);
                posts.push({ id: doc.id, post: doc.data() })
            }

        }





        posts.sort((a, b) =>{
            return a.timestamp-b.timestamp;
        })
        console.log("SENDING BACK " + JSON.stringify(posts));
        return { posts }

    } else {
        return []
    }



})

exports.getSubscriptions = functions.https.onCall(async (data, context) => {

    var username = context.auth.uid;


    
        var userRef = await admin.firestore().collection('User').doc(username).collection('Subscriptions').get();
        var userData = await userRef.docs;

        var subscriptions = [];
        userData.forEach((doc) => {
            subscriptions.push(doc.data())
        })
        console.log(userData)
        return { status: 200, subscriptions }
    
})

exports.saveAddress = functions.https.onCall(async (data, context) => {
    const username = context.auth.uid;
    const address = data;

    const userRef = await admin.firestore().doc('User/' + username).get();
    const user = userRef.data();

    const customer = await stripe.customers.update(
        user.stripeId,
        {address: address}
    )
    
    if(customer.id){
        return{status: 200, address}
    } else {
        return{status: 404, message:"Something went wrong while trying to update customer address"}
    }
})

exports.getPostsForUser = functions.https.onCall(async (data, context) => {
    var obj = data;
    var username = obj.username;
    var requestor = context.auth.uid;
    var requestorRef = await admin.firestore().collection('User').doc(requestor).collection('Subscriptions').get();
    var subscriptions = requestorRef.docs;
    var isSubscribed = false;

    for (const sub of subscriptions) {
        const subData = sub.data();
        if (subData.username === username) {
            isSubscribed = true;
        }
    }

    if (isSubscribed || username === requestor) {
        var postsRef = await admin.firestore().collection('User').doc(username).collection('Posts').orderBy('timestamp', 'desc').get().then(async (ref) => {
            if (ref.docs.length === 0) {
                return [];
            } else {
                var posts = []
                for (const post of ref.docs) {
                    posts.push({ id: post.id, post: post.data() })
                }
                return posts

            }
        }).catch(e => {
            return ({ status: "UserNotFound", message: "This Creator who does not exist" });
        })
        console.log(postsRef)
        return postsRef !== null ? { status: 200, posts: postsRef } : { status: "PostsNotFound", message: "The Creator has no posts" };
    } else {
        var postsRef = await admin.firestore().collection('User').doc(username).collection('Posts').orderBy('timestamp', 'desc').get().then(async (ref) => {
            if (ref.docs.length === 0) {
                return [];
            } else {
                var posts = []
                for (const post of ref.docs) {
                    const postData = post.data();
                    postData.media && (postData.media = "https://dummyimage.com/1920x1080/000/ffffff&text=You+have+to+be+subscribed+to+this+creator+to+view+this+post.")
                    post.type !== "image" && (post.type = "image")
                    console.log(postData)
                    posts.push({ id: post.id, post: postData })
                }
                return posts

            }
        }).catch(e => {
            return ({ status: "UserNotFound", message: "This Creator who does not exist" });
        })
        console.log(postsRef)
        return postsRef !== null ? { status: 200, posts: postsRef } : { status: "PostsNotFound", message: "The Creator has no posts" };
    }

    

    return requestorRef;


})


exports.getSubscriptionPrice = functions.https.onCall(async (data, context) => {
    const username = data.username;
    const user = await admin.firestore().doc(`User/${username}`).get()
    const priceId = user.data().creatorSettings.subscriptionpriceID;
    const price = await stripe.prices.retrieve(priceId)
    console.log(price)
    if (price) {
        return ({ price })
    } else {
        return ({ status: "CreatorNotFound", message: "The creator for which a price was requested does not exist" })
    }


})


exports.getNotifications = functions.https.onCall(async(data,context) => {
    const username = context.auth.uid;

    var notificationsArray = [];


    const notificationsRef = await admin.firestore().collection('User').doc(username).collection('Notifications').orderBy("timestamp", "desc").get();
    const notifications = notificationsRef.forEach((notification) => {
        notificationsArray.push( notification.data())
    });

    

    if(notificationsArray.length >0){
        return({ status:200, notifications:notificationsArray})
    } else {
        return({status:404, message:"No notifications found"})
    }
})

const createNotification = async ({type, issuer, recepient, image, content,timestamp, commentText}) => {
    const notification = {
        type:type || null,
        issuer:issuer || null,
        recepient:recepient || null,
        image:image || null,
        content:content || null,
        commentText:commentText || null,
        timestamp:timestamp,
    }
    var id = crypto.randomBytes(30).toString('hex');
    const res = await admin.firestore().collection('User').doc(recepient).collection('Notifications').doc(id).create(notification)

    if(res){
        return {status: 'success'};
    } else {
        return {status: 'error'};
    }
}


exports.subscribe = functions.https.onCall(async (data, context) => {
    const username = data.username;
    const uid = context.auth.uid;
    

    if (true) {
        var creaorRes = await admin.firestore().collection('User').doc(username).get();
        var userdata = creaorRes.data();

        var customerRes = await admin.firestore().collection('User').doc(uid).get();
        var customerData = customerRes.data();

        const timestamp = Math.floor(Date.now()/1000);

        try {
            const sub = await stripe.subscriptions.create({
                customer: customerData.stripeId,
                items: [{ 
                        price: userdata.creatorSettings.subscriptionpriceID
                    }],
                expand: ["latest_invoice.payment_intent"],
                application_fee_percent: 15,
                transfer_data: {
                    destination: userdata.creatorSettings.stripeID,
                  },
                
            });
            console.log(sub)
            if (sub.id) {
                var id = crypto.randomBytes(30).toString('hex');
                await admin.firestore().collection('User').doc(uid).collection('Subscriptions').doc(id).create({stripeId:sub.id,username:username,timestamp:timestamp})
                
                const notification = {
                    type:"subscription",
                    issuer:uid,
                    recepient:username,
                    image:customerData.profilepicture,
                    content:" has subscribed to you",
                    timestamp:timestamp,
                }
                

                var notificationRes = await createNotification(notification);
                console.log(notificationRes)
                if(notificationRes.status === "success"){
                    return { status: 200, sub_id: sub.id }
                } else {
                    
                    console.error("Notification could not be created for user " + username + " from issuer " + uid)
                    
                }
    
                return { status: 200, sub_id: sub.id }
            } else {
                return ({ status: 403, message: "Something went wrong. Please try again later or contact support" })
            }
        } catch (e) {
            if (e.code === "resource_missing") {
                return ({ status: "PaymentResourceMissing", message: "This User does not have a payment method attatched to their account. Please attach a payment method to continue." })
            }
        }


        


        


    } else {
        return ({ status: "UserNotAuthorized", message: "The requesting user is not authorized create a subscription like this" })
    }
})

exports.getPostComments = functions.https.onCall(async (data, context) => {
    const postId = data.postId;
    const creatorId = data.creatorId;
    console.log(postId + " " + creatorId)
    var commentsRef = await admin.firestore().collection('User').doc(creatorId).collection('Posts').doc(postId).collection('comments').get();
    var comments = []; 
    commentsRef.docs.forEach((doc) => {
        comments.push({id:doc.id, comment:doc.data()});
    });

    if(comments){
        return {status:200, comments:comments}
    } else {
        return {status:404, message:"Something went wrong while trying to access comments on this post"}
    }
})

exports.getCommentLikes = functions.https.onCall(async (data, context) => {
    var commentId = data.commentId;
    var creatorId = data.creatorId;
    var postId = data.postId;
    var commentsRef = await admin.firestore().collection('User').doc(creatorId).collection('Posts').doc(postId).collection('comments').doc(commentId).get();
    var likes = [];
    /*commentsRef.docs.forEach((doc) => {
        likes.push({id:doc.id, like:doc.data()})
    })*/

    likes = commentsRef.data().likes;


    if(likes){
        return ({ status:200, likes:likes})
    } else {
        return ({status:500, message:"something went wrong while trying to fetch likes on this comment"})
    }
    
})

exports.likeComment = functions.https.onCall(async (data, context) => {
    const uid = context.auth.uid;
    const commentId = data.commentId;
    const postId = data.postId;
    const postUser = data.postUser;

    const likeUser = await admin.firestore().collection('User').doc(uid).get();
    const userData = likeUser.data();
    const doc = await admin.firestore().collection('User').doc(postUser).collection('Posts').doc(postId).collection('comments').doc(commentId);
    const timestamp = Math.floor(Date.now()/1000);
        
        
    
    const newLike = {username:uid};
    console.log(newLike)
    const res = doc.update({
        "likes":admin.firestore.FieldValue.arrayUnion(newLike)
     })

    if(newLike){
        const notification = {
            type:"like",
            issuer:uid,
            recepient:postUser,
            image:userData.profilepicture,
            content:" has liked your comment \"" + (await doc.get()).data().content + "\"",
            timestamp:timestamp,
        }

        const res = await createNotification(notification)
        return {status:200, message:"Comment with id " + commentId + "on post " + postId + " was liked"}
    } else {
        return {status:500, message:"Something went wrong while trying to like this comment. Please try again later"}
    }
})

exports.unLikeComment = functions.https.onCall(async (data, context) => {
    const doc = await admin.firestore().collection('User').doc(data.postUser).collection('Posts').doc(data.postId).collection("comments").doc(data.commentId);
    
        
    const liked = {username:context.auth.uid};
    console.log(liked)
    console.log(data.postId)
        
   
    const res = await doc.update({
        "likes":admin.firestore.FieldValue.arrayRemove(liked)
     }).then(() => {
        return {status:200, message:"Comment with id " + data.commentId + " was unliked"}
     }).catch(() => {
        return {status:500, message:"Something went wrong while trying to like this post. Please try again later"}
     })

     return res;
})

exports.createComment = functions.https.onCall(async(data, context) => {
    const userId = context.auth.uid;
    const content = data.content
    const timestamp = Math.floor(new Date()/1000);
    const postId = data.postId;
    const creatorId = data.creatorId;
    var id = crypto.randomBytes(30).toString('hex');
    const comment = {
        userId,
        content,
        timestamp,
        postId,
        creatorId,
    }

    console.log(comment)

    const postRef = await admin.firestore().collection('User').doc(creatorId).collection('Posts').doc(postId).collection("comments").doc(id);

    const res = await postRef.set(comment)
    const notification = {
        type:"comment",
        issuer:userId,
        recepient:creatorId,
        commentText:content,
        content:" has commented on your post \"" + (await admin.firestore().collection('User').doc(creatorId).collection('Posts').doc(postId).get()).data().title + "\"",
        timestamp:timestamp,
    }

    const notificationRes = await createNotification(notification)
    if(res){
        return ({status:200, comment, id})
    } else {
        return ({status: 500, message:"Something went wrong while trying to post comment. Please try again later."})
    }

})

exports.createPost = functions.https.onCall((data, context) => {
    var post = data.post
    if (context.auth.uid) {
        var id = crypto.randomBytes(30).toString('hex');
        admin.firestore().collection('User').doc(post.username).collection('Posts').doc(id).create(post)
    } else {
        return "Not allowed"
    }
})

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
